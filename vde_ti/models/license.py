# -*- coding: utf-8 -*-
# ##############################################################################
#

#    OpenERP, Open Source Management Solution
#    Copyright (C) 2012-Today Acespritech Solutions Pvt Ltd
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

##############################################################################

from openerp import fields,models,api,_
import datetime
from openerp.exceptions import except_orm, Warning, RedirectWarning
from odoo.exceptions import UserError, AccessError
from odoo import _
import odoo.addons.decimal_precision as dp

class tilicense(models.Model):

    _name = 'it.license'
    _inherit = ['mail.thread']
    _sql_constraints = [
        ('it_license_unique_name', 'unique (num_license, license_aplicate)','La licencia debe ser unica!')]

    name = fields.Many2one('it.client', string="Cliente", required="True")
    modulo = fields.Selection([
        ('1', 'cash flow'),
        ('2', 'multicompany'),
        ('3', 'pdf maker'),
        ('4', ' report'),
        ('5', 'duplicate'),
        ('6', 'masked inpud'),
        ('7', 'dinamic blocks'),
        ('8', 'dinamic blocks '),
        ('9', 'gant '),
        ])
    Version = fields.Selection([
        ('1', '6.4'),
        ('2', '7'),
        ])
    compra = fields.Char(string="Compra")

    num_license = fields.Char(string="No. de licencia", required="True")

    license_aplicate = fields.Char(string="Licencia aplicada", required="True")

    Tipo = fields.Selection([
        ('1', 'Aplicada'),
        ('2', 'Reutilizada'),
        ])

    Servidor = fields.Selection([
        ('1', 'Sin acceso'),
        ('2', 'VDE'),
        ])

    Estatus = fields.Selection([
        ('1', 'Activa'),
        ('2', 'Desactiva'),
        ])

    date_aplicate = fields.Date(string="Fecha de aplicación")

    client_before = fields.Many2one('it.client', string="Cliente que pertenecio")

    server=fields.Many2one('it.host',string="Servidor de VDE", related="virtual.host_id")
    virtual=fields.Many2one('it.vm',string="Maquina virtual")


