# -*- coding: utf-8 -*-
# ##############################################################################
#

#    OpenERP, Open Source Management Solution
#    Copyright (C) 2012-Today Acespritech Solutions Pvt Ltd
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import fields,models,api,_
import datetime
from openerp.exceptions import except_orm, Warning, RedirectWarning
from odoo.exceptions import UserError, AccessError
from odoo import _
import odoo.addons.decimal_precision as dp

class itRequestVm(models.Model):

    _name = 'it.request.vm'
    _inherit = ['mail.thread']
    #_sql_constraints = [
        #('it_ip_unique_name', 'unique (name)','La ip debe ser unica!')]
    vm_id = fields.Many2one('it.vm', string="VM")
    #vm_ip = fields.Many2one('it.ip', string="ip", related="vm_id.ip_id")
    name = fields.Char(string="Solicitud")
    user_id = fields.Many2one('res.users',string="Solicitado por")
    assigned_user_id = fields.Many2one('res.users',string="Asignado a")
    so = fields.Char(string="Sistema Operativo")
    ram = fields.Float(string="RAM (GB)")
    hd = fields.Float(string="Disco Duro (GB)")
    note = fields.Float(string="Descripcion")
    state = fields.Selection([
        ('draft', 'Borrador'),
        ('sent', 'Enviada'),
        ('done', 'Generada'),
        ('cancel', 'Cancelada')
        ],default="draft")

