# -*- coding: utf-8 -*-
# ##############################################################################
#

#    OpenERP, Open Source Management Solution
#    Copyright (C) 2012-Today Acespritech Solutions Pvt Ltd
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import fields,models,api,_
import datetime
from openerp.exceptions import except_orm, Warning, RedirectWarning
from odoo.exceptions import UserError, AccessError
from odoo import _
import odoo.addons.decimal_precision as dp

class itVm(models.Model):

    _name = 'it.vm'
    _inherit = ['mail.thread']
    #_sql_constraints = [
        #('it_ip_unique_name', 'unique (name)','La ip debe ser unica!')]

    name = fields.Char(string="Nombre")
    depto_id = fields.Many2one('hr.department', string="Departamento")
    ip_id = fields.Many2one('it.ip', string="IP")
    section_id = fields.Many2one('it.section', string="Seccion", related="ip_id.section_id")
    subnet_id = fields.Many2one('it.subnet', string="Subnet", related="ip_id.subnet_id")
    vm_partner_ids = fields.One2many('it.vm.partner', 'vm_id', string="Cliente")
    host_id = fields.Many2one('it.host', string="Equipo")
    user = fields.Char(string="User")
    pwd = fields.Char(string="Password")

    so = fields.Char(string="SO")
    cpu = fields.Char(string="CPU")
    ram = fields.Integer(string="GB RAM")
    storage = fields.Integer(string="GB Storage")
    state = fields.Selection([
        ('active', 'Activa'),
        ('inactive', 'Inactiva'),
        ('deleted', 'Borrada')
        ],default="active")

    @api.multi
    def write(self, vals):
        #print "esto es vals ", vals
        old_ip = self.ip_id
        #print "esto es old_ip ", old_ip
        result =  super(models.Model, self).write(vals)
        #print "esto es new_ip ", vals.get('ip_id')
        if result:
            old_ip.write({'state': 'unused'})
            self.env['it.ip'].browse(vals.get('ip_id')).write({'state': 'used'})
        return result

    @api.model
    def create(self, vals):
        #print "entra create"
        ip = self.env['it.ip'].browse(vals.get('ip_id'))
        #print "esto es ip ", ip
        ip.write({'state': 'used'})
        host = super(models.Model, self).create(vals)
        return host

class tiVmPartner(models.Model):

    _name = 'it.vm.partner'

    vm_id = fields.Many2one('it.vm', stirng="VM")
    partner_id = fields.Many2one('res.partner', string="Cliente", domain="[('customer','=',True)]")
    app = fields.Selection([
        ('odoo', 'Odoo'),
        ('vtiger', 'Vtiger'),
        ('contpaq', 'Contpaq i'),
        ('dev', 'Desarrollo'),
        ('FTP', 'FTP'),
        ('otro', 'Otro'),
        ])
    port_ext = fields.Char(string="Puerto E.")
    port_int = fields.Char(string="Puerto I.")

    #netmask_id = fields.Many2one(string="Netmask", related="subnet_id.netmask_id")
    #note = fields.Char(string="Descripcion")

    #subnet_id = fields.Many2one('it.subnet', string="Subnet")
    #state = fields.Selection([
        #('unused', 'Libre'),
        #('used', 'Asignada')
        #])