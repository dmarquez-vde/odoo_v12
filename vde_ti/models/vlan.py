# -*- coding: utf-8 -*-
# ##############################################################################
#

#    OpenERP, Open Source Management Solution
#    Copyright (C) 2012-Today Acespritech Solutions Pvt Ltd
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import fields,models,api,_
import datetime
from openerp.exceptions import except_orm, Warning, RedirectWarning
from odoo.exceptions import UserError, AccessError
from odoo import _
import odoo.addons.decimal_precision as dp

class tiVlan(models.Model):

    _name = 'it.vlan'
    _inherit = ['mail.thread']

    _sql_constraints = [
         ('it_vlan_unique_code', 'unique (code)','El codigo de la vlan debe ser unico!')]

    name = fields.Char(string="Nombre")
    code = fields.Integer(string="Num")
    note = fields.Text(string="Descripcion")
    section_id = fields.Many2one('it.section', string="Seccion")
    subnet_ids = fields.One2many('it.subnet', 'vlan_id', string="Sub-redes")