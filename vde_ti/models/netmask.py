# -*- coding: utf-8 -*-
# ##############################################################################
#

#    OpenERP, Open Source Management Solution
#    Copyright (C) 2012-Today Acespritech Solutions Pvt Ltd
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import fields,models,api,_
import datetime
from openerp.exceptions import except_orm, Warning, RedirectWarning
from odoo.exceptions import UserError, AccessError
from odoo import _
import odoo.addons.decimal_precision as dp



class tiNetmask(models.Model):

    _name = "it.netmask"

    _sql_constraints = [
        ('it_netmask_unique_code', 'unique (code)','La netmask debe ser unica!')]

    @api.one
    @api.depends('code')
    def _get_available_hosts(self):
        masks = {
            32: '255.255.255.255',
            31: '255.255.255.254',
            30: '255.255.255.252',
            29: '255.255.255.248',
            28: '255.255.255.240',
            27: '255.255.255.224',
            26: '255.255.255.192',
            25: '255.255.255.128',
            24: '255.255.255.0',
            23: '255.255.254.0',
            22: '255.255.252.0',
            21: '255.255.248.0',
            20: '255.255.240.0',
            19: '255.255.224.0',
            18: '255.255.192.0',
            17: '255.255.128.0',
            16: '255.255.0.0',
            15: '255.254.0.0',
            14: '255.252.0.0',
            13: '255.248.0.0',
            12: '255.240.0.0',
            11: '255.224.0.0',
            10: '255.192.0.0',
            9: '255.128.0.0',
            8: '255.0.0.0',
            7: '254.0.0.0',
            6: '252.0.0.0',
            5: '248.0.0.0',
            4: '240.0.0.0',
            3: '224.0.0.0',
            2: '192.0.0.0',
            1: '128.0.0.0',
            0: '0.0.0.0'
            }
        self.name = masks.get(self.code)
        self.available_host = (pow(2, 32 - self.code)) - 2

    code = fields.Selection([
        (0, 0),
        (1, 1),
        (2, 2),
        (3, 3),
        (4, 4),
        (5, 5),
        (6, 6),
        (7, 7),
        (8, 8),
        (9, 9),
        (10, 10),
        (11, 11),
        (12, 12),
        (13, 13),
        (14, 14),
        (15, 15),
        (16, 16),
        (17, 17),
        (18, 18),
        (19, 19),
        (20, 20),
        (21, 21),
        (22, 22),
        (23, 23),
        (24, 24),
        (25, 25),
        (26, 26),
        (27, 27),
        (28, 28),
        (29, 29),
        (30, 30),
        (31, 31),
        (32, 32),
        ], string="CIDR")

    name = fields.Char(string="Mask", compute=_get_available_hosts, store=True)
    available_host = fields.Integer(string="Host disponibles",
         compute=_get_available_hosts, store=True)