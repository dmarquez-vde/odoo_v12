# -*- coding: utf-8 -*-
# ##############################################################################
#

#    OpenERP, Open Source Management Solution
#    Copyright (C) 2012-Today Acespritech Solutions Pvt Ltd
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import fields,models,api,_
import datetime
from openerp.exceptions import except_orm, Warning, RedirectWarning
from odoo.exceptions import UserError, AccessError
from odoo import _
import odoo.addons.decimal_precision as dp

class itDomain(models.Model):

    _name = 'it.domain'
    _inherit = ['mail.thread']

    name = fields.Char(string="Dominio")
    url = fields.Char(string="URL Cpanel")
    user = fields.Char(string="Usuario Cpanel")
    password = fields.Char(string="Password Cpanel")
    service_id = fields.Many2one('it.service', string="Servicio")
    fecha_registro = fields.Date(string="Fecha registro")
    fecha_vencimiento = fields.Date(string="Fecha Vencimiento")
    state = fields.Selection([
        ('active', 'Activa'),
        ('inactive', 'Inactiva'),
        ],default="active")