# -*- coding: utf-8 -*-
##############################################################################
#

#    OpenERP, Open Source Management Solution
#    Copyright (C) 2012-Today Acespritech Solutions Pvt Ltd
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'VDE TI',
    'version': '10.0.1',
    'category': 'Project Management',
    'description': """
Gives the base to manage TI area for companies.
=========================================================================================================

More information:
    """,
    'author': 'VDE Suite',
    'website': 'http://www.vde-suite.com',
    'depends': ['base', 'hr'],
    'data': [
        "security/vde_ti.xml",
        "security/ir.model.access.csv",
        'views/vde_ti.xml',
        'views/subnet.xml',
        'views/ip.xml',
        'views/host.xml',
        'views/vm.xml',
        'views/sections.xml',
        'views/vlan.xml',
        'views/service.xml',
        'views/domain.xml',
        'views/wifi.xml',
        'views/client.xml',
        'views/license.xml',
        'views/request.xml',

       ],
    #'external_dependencies': {
        #'python' : ['bs4'],
    #},
    #'demo': ['data/project_scrum_demo.xml'],
    'installable': True,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
