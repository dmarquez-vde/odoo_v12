# -*- coding: utf-8 -*-
##############################################################################
#

#    OpenERP, Open Source Management Solution
#    Copyright (C) 2012-Today Acespritech Solutions Pvt Ltd
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'VDE SO DISCOUNT',
    'version': '12.0.1',
    'category': 'Sales',
    'description': """
Add total discount in SO form.
=========================================================================================================

More information:
    """,
    'author': 'VDE Suite',
    'website': 'http://www.vde-suite.com',
    'depends': ['base', 'sale_management'],
    'data': [
        'views/sale_order_view.xml',
       ],
    #'external_dependencies': {
        #'python' : ['bs4'],
    #},
    #'demo': ['data/project_scrum_demo.xml'],
    'installable': True,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
