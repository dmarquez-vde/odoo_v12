# -*- coding: utf-8 -*-
# ##############################################################################
#

#    OpenERP, Open Source Management Solution
#    Copyright (C) 2012-Today Acespritech Solutions Pvt Ltd
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from odoo import models, fields, api, _
from odoo.exceptions import ValidationError


class SaleReservation(models.TransientModel):
    _name = "sale.reservation"

    sale_order_id = fields.Many2one(
        'sale.order',
        string='Cotizacion',
        readonly=True,
    )
    reservation_line_ids = fields.One2many(
        'sale.reservation.line',
        'reservation_id',
        string='Lineas de reservacion',
    )
    user_ids = fields.Many2many(
        'res.users',
        string='Notificacion via Email',
    )

    @api.model
    def default_get(self,fields):
        res = super(SaleReservation, self).default_get(fields)
        vals = []
        order_id = self._context.get('active_id')
        print ("ids ", order_id)
        lines = self.env['sale.order.line'].search([('order_id', '=', order_id)])
        print ("lines ", lines)
        for line in lines:
            if line.reserved_qty < line.product_uom_qty:
                vals.append((0, 0, {
                    'order_id': order_id,
                    'order_line_id': line.id,
                    'product_id': line.product_id.id,
                    'product_qty': line.product_uom_qty - line.reserved_qty,
                    'uom_id': line.product_uom.id,
                    'stock_reservation_qty': line.product_uom_qty- line.reserved_qty,
                    }))
        print ("vals ", vals)
        #if len(vals) < 1:
            #raise ValidationError("No hay productos pendientes de reservar")
        res.update({'reservation_line_ids': vals})
        return res

    @api.multi
    def action_create_reservation(self):
        self.ensure_one()

        order_id = self._context.get('active_id')
        print ("esto es order_id",order_id)
        algo=self.env['sale.order'].search([('id', '=', order_id)])
        for algo1 in algo:
            if not algo1.start_date or not algo1.end_date:
                raise ValidationError("No se han asignado fecha inicio y fecha fin")

        
        domain = {}
        quote_obj = self.env['sale.order.quote']
        for line in self.reservation_line_ids:
            ambos = []
            f1 = []
            f2 = []
            existencias = line.product_id.qty_sent
            print ("existencias generales ", str(existencias))
            quote_ids = self.env['sale.order.quote'].search([
                ('product_id','=',line.product_id.id),
                ('state','in',['draft','returned','assigned','waiting_for_return','done']),
                ('start_date','!=',False),
                ('end_date','!=',False),
                ])
            print ("quote_ids ", quote_ids)
            for quote in quote_ids:
                #=ambos Entre=
                #f1 >= F1 and f1 <= F2
                if (line.order_line_id.d1 >= quote.d1) and (line.order_line_id.d2 <=quote.d2):
                    ambos.append(quote)
                #=solo f1 entre=
                #f1 >= F1 and f1 <= F2 and f2 > F2
                if (line.order_line_id.d1 >= quote.d1) and (line.order_line_id.d1 <= quote.d2) and (line.order_line_id.d2 > quote.d2):
                    f1.append(quote)
                #=solo f2 entre=
                #f2 <= F2 and f2 >= F1 and f1 < F1
                if (line.order_line_id.d2 <= quote.d2) and (line.order_line_id.d2 >= quote.d1) and (line.order_line_id.d1 < quote.d1):
                    f2.append(quote)


            print ("ambos ", ambos)
            print ("f1 ", f1)
            print ("f2 ", f2)

            if len(ambos)>0:
                cant = 0
                for item in ambos:
                    print ("Esto es item",item)
                    cant = cant + item.qty
                print ("cant in ambos", cant)
                print ("line.stock_reservation_qty", line.stock_reservation_qty)
                print ("existencias",existencias)
                existencias_no_reservadas = existencias - cant
                print ("existencias no reservadas",existencias_no_reservadas)

                if existencias_no_reservadas <= 0:
                    state = 'waiting'
                    if line.stock_reservation_qty > 0.0:
                        order_line_reserve_qty = line.order_line_id.reserved_qty
                        order_line_reserve_qty += line.stock_reservation_qty
                        if 1==1:
                            quote_id = quote_obj.create({
                                'order_id': line.sale_order_id.id,
                                'line_id': line.order_line_id.id,
                                'product_id': line.product_id.id,
                                'product_uom': line.uom_id.id,
                                'name': line.order_line_id.name,
                                'qty': line.stock_reservation_qty,
                                'state': state
                                })
                            if quote_id:
                                print ("la orden ", line.order_line_id.order_id)
                                line.order_line_id.order_id.write({'is_stock_reserv_created': True})
                                if state=='draft':
                                    line.order_line_id.write({'is_reserved': True, 'reserved_qty': order_line_reserve_qty, 'quote_id': quote_id.id})
                                else:
                                    line.order_line_id.write({'quote_id': quote_id.id})
                        else:
                            raise ValidationError("Ya ha sido reservado completamente el producto " + line.order_line_id.name)
                    else:
                        raise ValidationError("La cantidad a reservar del producto "+line.order_line_id.name+" debe ser mayor a 0")

                else:

                    if line.stock_reservation_qty > 0.0:
                        if line.stock_reservation_qty <= existencias_no_reservadas:
                            state = 'draft'
                            if line.stock_reservation_qty > 0.0:
                                order_line_reserve_qty = line.order_line_id.reserved_qty
                                order_line_reserve_qty += line.stock_reservation_qty
                                if 1==1:
                                    quote_id = quote_obj.create({
                                        'order_id': line.sale_order_id.id,
                                        'line_id': line.order_line_id.id,
                                        'product_id': line.product_id.id,
                                        'product_uom': line.uom_id.id,
                                        'name': line.order_line_id.name,
                                        'qty': line.stock_reservation_qty,
                                        'state': state
                                        })
                                    if quote_id:
                                        print ("la orden ", line.order_line_id.order_id)
                                        line.order_line_id.order_id.write({'is_stock_reserv_created': True})
                                        if state=='draft':
                                            line.order_line_id.write({'is_reserved': True, 'reserved_qty': order_line_reserve_qty, 'quote_id': quote_id.id})
                                        else:
                                            line.order_line_id.write({'quote_id': quote_id.id})
                                else:
                                    raise ValidationError("Ya ha sido reservado completamente el producto " + line.order_line_id.name)
                            else:
                                raise ValidationError("La cantidad a reservar del producto "+line.order_line_id.name+" debe ser mayor a 0")

                        else:
                            state = 'waiting'

                            canti = line.stock_reservation_qty - existencias_no_reservadas

                            order_line_reserve_qty = line.order_line_id.reserved_qty
                            order_line_reserve_qty += line.stock_reservation_qty
                            if 1==1:
                                quote_id = quote_obj.create({
                                    'order_id': line.sale_order_id.id,
                                    'line_id': line.order_line_id.id,
                                    'product_id': line.product_id.id,
                                    'product_uom': line.uom_id.id,
                                    'name': line.order_line_id.name,
                                    'qty': canti,
                                    'state': state
                                    })
                                if quote_id:
                                    print ("la orden ", line.order_line_id.order_id)
                                    line.order_line_id.order_id.write({'is_stock_reserv_created': True})
                                    if state=='draft':
                                        line.order_line_id.write({'is_reserved': True, 'reserved_qty': order_line_reserve_qty, 'quote_id': quote_id.id})
                                    else:
                                        line.order_line_id.write({'quote_id': quote_id.id})
                            else:
                                raise ValidationError("Ya ha sido reservado completamente el producto " + line.order_line_id.name)

                            state = 'draft'
                            order_line_reserve_qty = line.order_line_id.reserved_qty
                            order_line_reserve_qty += line.stock_reservation_qty
                            if 1==1:
                                quote_id = quote_obj.create({
                                    'order_id': line.sale_order_id.id,
                                    'line_id': line.order_line_id.id,
                                    'product_id': line.product_id.id,
                                    'product_uom': line.uom_id.id,
                                    'name': line.order_line_id.name,
                                    'qty': existencias_no_reservadas,
                                    'state': state
                                    })
                                if quote_id:
                                    print ("la orden ", line.order_line_id.order_id)
                                    line.order_line_id.order_id.write({'is_stock_reserv_created': True})
                                    if state=='draft':
                                        line.order_line_id.write({'is_reserved': True, 'reserved_qty': order_line_reserve_qty, 'quote_id': quote_id.id})
                                    else:
                                        line.order_line_id.write({'quote_id': quote_id.id})
                            else:
                                raise ValidationError("Ya ha sido reservado completamente el producto " + line.order_line_id.name)
                    















            elif len(f1)>0:
                cant = 0
                for item in f1:
                    print ("Esto es item",item)
                    cant = cant + item.qty
                print ("cant in f1", cant)
                print ("line.stock_reservation_qty", line.stock_reservation_qty)
                print ("existencias",existencias)
                existencias_no_reservadas = existencias - cant
                print ("existencias no reservadas",existencias_no_reservadas)

                if existencias_no_reservadas <= 0:
                    state = 'waiting'
                    if line.stock_reservation_qty > 0.0:
                        order_line_reserve_qty = line.order_line_id.reserved_qty
                        order_line_reserve_qty += line.stock_reservation_qty
                        if 1==1:
                            quote_id = quote_obj.create({
                                'order_id': line.sale_order_id.id,
                                'line_id': line.order_line_id.id,
                                'product_id': line.product_id.id,
                                'product_uom': line.uom_id.id,
                                'name': line.order_line_id.name,
                                'qty': line.stock_reservation_qty,
                                'state': state
                                })
                            if quote_id:
                                print ("la orden ", line.order_line_id.order_id)
                                line.order_line_id.order_id.write({'is_stock_reserv_created': True})
                                if state=='draft':
                                    line.order_line_id.write({'is_reserved': True, 'reserved_qty': order_line_reserve_qty, 'quote_id': quote_id.id})
                                else:
                                    line.order_line_id.write({'quote_id': quote_id.id})
                        else:
                            raise ValidationError("Ya ha sido reservado completamente el producto " + line.order_line_id.name)
                    else:
                        raise ValidationError("La cantidad a reservar del producto "+line.order_line_id.name+" debe ser mayor a 0")

                else:

                    if line.stock_reservation_qty > 0.0:
                        if line.stock_reservation_qty <= existencias_no_reservadas:
                            state = 'draft'
                            if line.stock_reservation_qty > 0.0:
                                order_line_reserve_qty = line.order_line_id.reserved_qty
                                order_line_reserve_qty += line.stock_reservation_qty
                                if 1==1:
                                    quote_id = quote_obj.create({
                                        'order_id': line.sale_order_id.id,
                                        'line_id': line.order_line_id.id,
                                        'product_id': line.product_id.id,
                                        'product_uom': line.uom_id.id,
                                        'name': line.order_line_id.name,
                                        'qty': line.stock_reservation_qty,
                                        'state': state
                                        })
                                    if quote_id:
                                        print ("la orden ", line.order_line_id.order_id)
                                        line.order_line_id.order_id.write({'is_stock_reserv_created': True})
                                        if state=='draft':
                                            line.order_line_id.write({'is_reserved': True, 'reserved_qty': order_line_reserve_qty, 'quote_id': quote_id.id})
                                        else:
                                            line.order_line_id.write({'quote_id': quote_id.id})
                                else:
                                    raise ValidationError("Ya ha sido reservado completamente el producto " + line.order_line_id.name)
                            else:
                                raise ValidationError("La cantidad a reservar del producto "+line.order_line_id.name+" debe ser mayor a 0")

                        else:
                            state = 'waiting'

                            canti = line.stock_reservation_qty - existencias_no_reservadas

                            order_line_reserve_qty = line.order_line_id.reserved_qty
                            order_line_reserve_qty += line.stock_reservation_qty
                            if 1==1:
                                quote_id = quote_obj.create({
                                    'order_id': line.sale_order_id.id,
                                    'line_id': line.order_line_id.id,
                                    'product_id': line.product_id.id,
                                    'product_uom': line.uom_id.id,
                                    'name': line.order_line_id.name,
                                    'qty': canti,
                                    'state': state
                                    })
                                if quote_id:
                                    print ("la orden ", line.order_line_id.order_id)
                                    line.order_line_id.order_id.write({'is_stock_reserv_created': True})
                                    if state=='draft':
                                        line.order_line_id.write({'is_reserved': True, 'reserved_qty': order_line_reserve_qty, 'quote_id': quote_id.id})
                                    else:
                                        line.order_line_id.write({'quote_id': quote_id.id})
                            else:
                                raise ValidationError("Ya ha sido reservado completamente el producto " + line.order_line_id.name)

                            state = 'draft'
                            order_line_reserve_qty = line.order_line_id.reserved_qty
                            order_line_reserve_qty += line.stock_reservation_qty
                            if 1==1:
                                quote_id = quote_obj.create({
                                    'order_id': line.sale_order_id.id,
                                    'line_id': line.order_line_id.id,
                                    'product_id': line.product_id.id,
                                    'product_uom': line.uom_id.id,
                                    'name': line.order_line_id.name,
                                    'qty': existencias_no_reservadas,
                                    'state': state
                                    })
                                if quote_id:
                                    print ("la orden ", line.order_line_id.order_id)
                                    line.order_line_id.order_id.write({'is_stock_reserv_created': True})
                                    if state=='draft':
                                        line.order_line_id.write({'is_reserved': True, 'reserved_qty': order_line_reserve_qty, 'quote_id': quote_id.id})
                                    else:
                                        line.order_line_id.write({'quote_id': quote_id.id})
                            else:
                                raise ValidationError("Ya ha sido reservado completamente el producto " + line.order_line_id.name)
                




































            elif len(f2)>0:
                cant = 0
                for item in f2:
                    print ("Esto es item",item)
                    cant = cant + item.qty
                print ("cant in F2", cant)
                print ("line.stock_reservation_qty", line.stock_reservation_qty)
                print ("existencias",existencias)
                existencias_no_reservadas = existencias - cant
                print ("existencias no reservadas",existencias_no_reservadas)

                if existencias_no_reservadas <= 0:
                    state = 'waiting'
                    if line.stock_reservation_qty > 0.0:
                        order_line_reserve_qty = line.order_line_id.reserved_qty
                        order_line_reserve_qty += line.stock_reservation_qty
                        if 1==1:
                            quote_id = quote_obj.create({
                                'order_id': line.sale_order_id.id,
                                'line_id': line.order_line_id.id,
                                'product_id': line.product_id.id,
                                'product_uom': line.uom_id.id,
                                'name': line.order_line_id.name,
                                'qty': line.stock_reservation_qty,
                                'state': state
                                })
                            if quote_id:
                                print ("la orden ", line.order_line_id.order_id)
                                line.order_line_id.order_id.write({'is_stock_reserv_created': True})
                                if state=='draft':
                                    line.order_line_id.write({'is_reserved': True, 'reserved_qty': order_line_reserve_qty, 'quote_id': quote_id.id})
                                else:
                                    line.order_line_id.write({'quote_id': quote_id.id})
                        else:
                            raise ValidationError("Ya ha sido reservado completamente el producto " + line.order_line_id.name)
                    else:
                        raise ValidationError("La cantidad a reservar del producto "+line.order_line_id.name+" debe ser mayor a 0")

                else:

                    if line.stock_reservation_qty > 0.0:
                        if line.stock_reservation_qty <= existencias_no_reservadas:
                            state = 'draft'
                            if line.stock_reservation_qty > 0.0:
                                order_line_reserve_qty = line.order_line_id.reserved_qty
                                order_line_reserve_qty += line.stock_reservation_qty
                                if 1==1:
                                    quote_id = quote_obj.create({
                                        'order_id': line.sale_order_id.id,
                                        'line_id': line.order_line_id.id,
                                        'product_id': line.product_id.id,
                                        'product_uom': line.uom_id.id,
                                        'name': line.order_line_id.name,
                                        'qty': line.stock_reservation_qty,
                                        'state': state
                                        })
                                    if quote_id:
                                        print ("la orden ", line.order_line_id.order_id)
                                        line.order_line_id.order_id.write({'is_stock_reserv_created': True})
                                        if state=='draft':
                                            line.order_line_id.write({'is_reserved': True, 'reserved_qty': order_line_reserve_qty, 'quote_id': quote_id.id})
                                        else:
                                            line.order_line_id.write({'quote_id': quote_id.id})
                                else:
                                    raise ValidationError("Ya ha sido reservado completamente el producto " + line.order_line_id.name)
                            else:
                                raise ValidationError("La cantidad a reservar del producto "+line.order_line_id.name+" debe ser mayor a 0")

                        else:
                            state = 'waiting'

                            canti = line.stock_reservation_qty - existencias_no_reservadas

                            order_line_reserve_qty = line.order_line_id.reserved_qty
                            order_line_reserve_qty += line.stock_reservation_qty
                            if 1==1:
                                quote_id = quote_obj.create({
                                    'order_id': line.sale_order_id.id,
                                    'line_id': line.order_line_id.id,
                                    'product_id': line.product_id.id,
                                    'product_uom': line.uom_id.id,
                                    'name': line.order_line_id.name,
                                    'qty': canti,
                                    'state': state
                                    })
                                if quote_id:
                                    print ("la orden ", line.order_line_id.order_id)
                                    line.order_line_id.order_id.write({'is_stock_reserv_created': True})
                                    if state=='draft':
                                        line.order_line_id.write({'is_reserved': True, 'reserved_qty': order_line_reserve_qty, 'quote_id': quote_id.id})
                                    else:
                                        line.order_line_id.write({'quote_id': quote_id.id})
                            else:
                                raise ValidationError("Ya ha sido reservado completamente el producto " + line.order_line_id.name)

                            state = 'draft'
                            order_line_reserve_qty = line.order_line_id.reserved_qty
                            order_line_reserve_qty += line.stock_reservation_qty
                            if 1==1:
                                quote_id = quote_obj.create({
                                    'order_id': line.sale_order_id.id,
                                    'line_id': line.order_line_id.id,
                                    'product_id': line.product_id.id,
                                    'product_uom': line.uom_id.id,
                                    'name': line.order_line_id.name,
                                    'qty': existencias_no_reservadas,
                                    'state': state
                                    })
                                if quote_id:
                                    print ("la orden ", line.order_line_id.order_id)
                                    line.order_line_id.order_id.write({'is_stock_reserv_created': True})
                                    if state=='draft':
                                        line.order_line_id.write({'is_reserved': True, 'reserved_qty': order_line_reserve_qty, 'quote_id': quote_id.id})
                                    else:
                                        line.order_line_id.write({'quote_id': quote_id.id})
                            else:
                                raise ValidationError("Ya ha sido reservado completamente el producto " + line.order_line_id.name)
                

























            else:
                if line.stock_reservation_qty > 0.0:
                    
                    if line.stock_reservation_qty <= existencias:
                        state = 'draft'
                        order_line_reserve_qty = line.order_line_id.reserved_qty
                        order_line_reserve_qty += line.stock_reservation_qty
                        if 1==1:
                            quote_id = quote_obj.create({
                                'order_id': line.sale_order_id.id,
                                'line_id': line.order_line_id.id,
                                'product_id': line.product_id.id,
                                'product_uom': line.uom_id.id,
                                'name': line.order_line_id.name,
                                'qty': line.stock_reservation_qty,
                                'state': state
                                })
                            if quote_id:
                                print ("la orden ", line.order_line_id.order_id)
                                line.order_line_id.order_id.write({'is_stock_reserv_created': True})
                                if state=='draft':
                                    line.order_line_id.write({'is_reserved': True, 'reserved_qty': order_line_reserve_qty, 'quote_id': quote_id.id})
                                else:
                                    line.order_line_id.write({'quote_id': quote_id.id})
                        else:
                            raise ValidationError("Ya ha sido reservado completamente el producto " + line.order_line_id.name)
                    else:
                        state = 'waiting'

                        canti = line.stock_reservation_qty - existencias

                        order_line_reserve_qty = line.order_line_id.reserved_qty
                        order_line_reserve_qty += line.stock_reservation_qty
                        if 1==1:
                            quote_id = quote_obj.create({
                                'order_id': line.sale_order_id.id,
                                'line_id': line.order_line_id.id,
                                'product_id': line.product_id.id,
                                'product_uom': line.uom_id.id,
                                'name': line.order_line_id.name,
                                'qty': canti,
                                'state': state
                                })
                            if quote_id:
                                print ("la orden ", line.order_line_id.order_id)
                                line.order_line_id.order_id.write({'is_stock_reserv_created': True})
                                if state=='draft':
                                    line.order_line_id.write({'is_reserved': True, 'reserved_qty': order_line_reserve_qty, 'quote_id': quote_id.id})
                                else:
                                    line.order_line_id.write({'quote_id': quote_id.id})
                        else:
                            raise ValidationError("Ya ha sido reservado completamente el producto " + line.order_line_id.name)

                        state = 'draft'
                        order_line_reserve_qty = line.order_line_id.reserved_qty
                        order_line_reserve_qty += line.stock_reservation_qty
                        if 1==1:
                            quote_id = quote_obj.create({
                                'order_id': line.sale_order_id.id,
                                'line_id': line.order_line_id.id,
                                'product_id': line.product_id.id,
                                'product_uom': line.uom_id.id,
                                'name': line.order_line_id.name,
                                'qty': existencias,
                                'state': state
                                })
                            if quote_id:
                                print ("la orden ", line.order_line_id.order_id)
                                line.order_line_id.order_id.write({'is_stock_reserv_created': True})
                                if state=='draft':
                                    line.order_line_id.write({'is_reserved': True, 'reserved_qty': order_line_reserve_qty, 'quote_id': quote_id.id})
                                else:
                                    line.order_line_id.write({'quote_id': quote_id.id})
                        else:
                            raise ValidationError("Ya ha sido reservado completamente el producto " + line.order_line_id.name)
                else:
                    raise ValidationError("La cantidad a reservar del producto "+line.order_line_id.name+" debe ser mayor a 0")














    @api.multi
    def action_reload_create_reservation(self):
        self.ensure_one()

        order_id = self._context.get('active_id')
        print ("esto es order_id",order_id)
        algo=self.env['sale.order'].search([('id', '=', order_id)])
        for algo1 in algo:
            if not algo1.start_date or not algo1.end_date:
                raise ValidationError("No se han asignado fecha inicio y fecha fin")

        
        domain = {}
        quote_obj = self.env['sale.order.quote']
        for line in quote_obj.search([('state','=','waiting'),('order_id','=',order_id)]):
            print ("Esto es line",line)
            ambos = []
            f1 = []
            f2 = []
            existencias = line.product_id.qty_sent
            print ("existencias generales ", str(existencias))
            quote_ids = self.env['sale.order.quote'].search([
                ('product_id','=',line.product_id.id),
                ('state','in',['draft','returned','assigned','waiting_for_return','done']),
                ('start_date','!=',False),
                ('end_date','!=',False),
                ])
            print ("quote_ids ", quote_ids)
            for quote in quote_ids:
                #=ambos Entre=
                #f1 >= F1 and f1 <= F2
                if (line.order_id.d1 >= quote.d1) and (line.order_id.d2 <=quote.d2):
                    ambos.append(quote)
                #=solo f1 entre=
                #f1 >= F1 and f1 <= F2 and f2 > F2
                if (line.order_id.d1 >= quote.d1) and (line.order_id.d1 <= quote.d2) and (line.order_id.d2 > quote.d2):
                    f1.append(quote)
                #=solo f2 entre=
                #f2 <= F2 and f2 >= F1 and f1 < F1
                if (line.order_id.d2 <= quote.d2) and (line.order_id.d2 >= quote.d1) and (line.order_id.d1 < quote.d1):
                    f2.append(quote)


            print ("ambos ", ambos)
            print ("f1 ", f1)
            print ("f2 ", f2)

            if len(ambos)>0:
                cant = 0
                for item in ambos:
                    print ("Esto es item",item)
                    cant = cant + item.qty
                print ("cant in ambos", cant)
                print ("existencias",existencias)
                existencias_no_reservadas = existencias - cant
                print ("existencias no reservadas",existencias_no_reservadas)

                if existencias_no_reservadas <= 0:
                    state = 'waiting'
                    if line.qty > 0.0:
                        order_line_reserve_qty = line.line_id.reserved_qty
                        order_line_reserve_qty += line.qty
                        if 1==1:
                            quote_id = quote_obj.create({
                                'order_id': line.order_id.id,
                                'line_id': line.line_id.id,
                                'product_id': line.product_id.id,
                                'product_uom': line.product_uom.id,
                                'name': line.line_id.name,
                                'qty': line.qty,
                                'state': state
                                })
                            if quote_id:
                                print ("la orden ", line.line_id.order_id)
                                line.line_id.order_id.write({'is_stock_reserv_created': True})
                                if state=='draft':
                                    line.line_id.write({'is_reserved': True, 'reserved_qty': order_line_reserve_qty, 'quote_id': quote_id.id})
                                else:
                                    line.line_id.write({'quote_id': quote_id.id})
                        else:
                            raise ValidationError("Ya ha sido reservado completamente el producto " + line.line_id.name)
                    else:
                        raise ValidationError("La cantidad a reservar del producto "+line.line_id.name+" debe ser mayor a 0")

                else:

                    if line.qty > 0.0:
                        if line.qty <= existencias_no_reservadas:
                            state = 'draft'
                            if line.qty > 0.0:
                                order_line_reserve_qty = line.line_id.reserved_qty
                                order_line_reserve_qty += line.qty
                                if 1==1:
                                    quote_id = quote_obj.create({
                                        'order_id': line.order_id.id,
                                        'line_id': line.line_id.id,
                                        'product_id': line.product_id.id,
                                        'product_uom': line.product_uom.id,
                                        'name': line.line_id.name,
                                        'qty': line.qty,
                                        'state': state
                                        })
                                    if quote_id:
                                        print ("la orden ", line.line_id.order_id)
                                        line.line_id.order_id.write({'is_stock_reserv_created': True})
                                        if state=='draft':
                                            line.line_id.write({'is_reserved': True, 'reserved_qty': order_line_reserve_qty, 'quote_id': quote_id.id})
                                        else:
                                            line.line_id.write({'quote_id': quote_id.id})
                                else:
                                    raise ValidationError("Ya ha sido reservado completamente el producto " + line.line_id.name)
                            else:
                                raise ValidationError("La cantidad a reservar del producto "+line.line_id.name+" debe ser mayor a 0")

                        else:
                            state = 'waiting'

                            canti = line.qty - existencias_no_reservadas

                            order_line_reserve_qty = line.line_id.reserved_qty
                            order_line_reserve_qty += line.qty
                            if 1==1:
                                quote_id = quote_obj.create({
                                    'order_id': line.order_id.id,
                                    'line_id': line.line_id.id,
                                    'product_id': line.product_id.id,
                                    'product_uom': line.product_uom.id,
                                    'name': line.line_id.name,
                                    'qty': canti,
                                    'state': state
                                    })
                                if quote_id:
                                    print ("la orden ", line.line_id.order_id)
                                    line.line_id.order_id.write({'is_stock_reserv_created': True})
                                    if state=='draft':
                                        line.line_id.write({'is_reserved': True, 'reserved_qty': order_line_reserve_qty, 'quote_id': quote_id.id})
                                    else:
                                        line.line_id.write({'quote_id': quote_id.id})
                            else:
                                raise ValidationError("Ya ha sido reservado completamente el producto " + line.line_id.name)

                            state = 'draft'
                            order_line_reserve_qty = line.line_id.reserved_qty
                            order_line_reserve_qty += line.qty
                            if 1==1:
                                quote_id = quote_obj.create({
                                    'order_id': line.order_id.id,
                                    'line_id': line.line_id.id,
                                    'product_id': line.product_id.id,
                                    'product_uom': line.product_uom.id,
                                    'name': line.line_id.name,
                                    'qty': existencias_no_reservadas,
                                    'state': state
                                    })
                                if quote_id:
                                    print ("la orden ", line.line_id.order_id)
                                    line.line_id.order_id.write({'is_stock_reserv_created': True})
                                    if state=='draft':
                                        line.line_id.write({'is_reserved': True, 'reserved_qty': order_line_reserve_qty, 'quote_id': quote_id.id})
                                    else:
                                        line.line_id.write({'quote_id': quote_id.id})
                            else:
                                raise ValidationError("Ya ha sido reservado completamente el producto " + line.line_id.name)
                    















            elif len(f1)>0:
                cant = 0
                for item in f1:
                    print ("Esto es item",item)
                    cant = cant + item.qty
                print ("cant in f1", cant)
                print ("existencias",existencias)
                existencias_no_reservadas = existencias - cant
                print ("existencias no reservadas",existencias_no_reservadas)

                if existencias_no_reservadas <= 0:
                    state = 'waiting'
                    if line.qty > 0.0:
                        order_line_reserve_qty = line.line_id.reserved_qty
                        order_line_reserve_qty += line.qty
                        if 1==1:
                            quote_id = quote_obj.create({
                                'order_id': line.order_id.id,
                                'line_id': line.line_id.id,
                                'product_id': line.product_id.id,
                                'product_uom': line.product_uom.id,
                                'name': line.line_id.name,
                                'qty': line.qty,
                                'state': state
                                })
                            if quote_id:
                                print ("la orden ", line.line_id.order_id)
                                line.line_id.order_id.write({'is_stock_reserv_created': True})
                                if state=='draft':
                                    line.line_id.write({'is_reserved': True, 'reserved_qty': order_line_reserve_qty, 'quote_id': quote_id.id})
                                else:
                                    line.line_id.write({'quote_id': quote_id.id})
                        else:
                            raise ValidationError("Ya ha sido reservado completamente el producto " + line.line_id.name)
                    else:
                        raise ValidationError("La cantidad a reservar del producto "+line.line_id.name+" debe ser mayor a 0")

                else:

                    if line.qty > 0.0:
                        if line.qty <= existencias_no_reservadas:
                            state = 'draft'
                            if line.qty > 0.0:
                                order_line_reserve_qty = line.line_id.reserved_qty
                                order_line_reserve_qty += line.qty
                                if 1==1:
                                    quote_id = quote_obj.create({
                                        'order_id': line.order_id.id,
                                        'line_id': line.line_id.id,
                                        'product_id': line.product_id.id,
                                        'product_uom': line.product_uom.id,
                                        'name': line.line_id.name,
                                        'qty': line.qty,
                                        'state': state
                                        })
                                    if quote_id:
                                        print ("la orden ", line.line_id.order_id)
                                        line.line_id.order_id.write({'is_stock_reserv_created': True})
                                        if state=='draft':
                                            line.line_id.write({'is_reserved': True, 'reserved_qty': order_line_reserve_qty, 'quote_id': quote_id.id})
                                        else:
                                            line.line_id.write({'quote_id': quote_id.id})
                                else:
                                    raise ValidationError("Ya ha sido reservado completamente el producto " + line.line_id.name)
                            else:
                                raise ValidationError("La cantidad a reservar del producto "+line.line_id.name+" debe ser mayor a 0")

                        else:
                            state = 'waiting'

                            canti = line.qty - existencias_no_reservadas

                            order_line_reserve_qty = line.line_id.reserved_qty
                            order_line_reserve_qty += line.qty
                            if 1==1:
                                quote_id = quote_obj.create({
                                    'order_id': line.order_id.id,
                                    'line_id': line.line_id.id,
                                    'product_id': line.product_id.id,
                                    'product_uom': line.product_uom.id,
                                    'name': line.line_id.name,
                                    'qty': canti,
                                    'state': state
                                    })
                                if quote_id:
                                    print ("la orden ", line.line_id.order_id)
                                    line.line_id.order_id.write({'is_stock_reserv_created': True})
                                    if state=='draft':
                                        line.line_id.write({'is_reserved': True, 'reserved_qty': order_line_reserve_qty, 'quote_id': quote_id.id})
                                    else:
                                        line.line_id.write({'quote_id': quote_id.id})
                            else:
                                raise ValidationError("Ya ha sido reservado completamente el producto " + line.line_id.name)

                            state = 'draft'
                            order_line_reserve_qty = line.line_id.reserved_qty
                            order_line_reserve_qty += line.qty
                            if 1==1:
                                quote_id = quote_obj.create({
                                    'order_id': line.order_id.id,
                                    'line_id': line.line_id.id,
                                    'product_id': line.product_id.id,
                                    'product_uom': line.product_uom.id,
                                    'name': line.line_id.name,
                                    'qty': existencias_no_reservadas,
                                    'state': state
                                    })
                                if quote_id:
                                    print ("la orden ", line.line_id.order_id)
                                    line.line_id.order_id.write({'is_stock_reserv_created': True})
                                    if state=='draft':
                                        line.line_id.write({'is_reserved': True, 'reserved_qty': order_line_reserve_qty, 'quote_id': quote_id.id})
                                    else:
                                        line.line_id.write({'quote_id': quote_id.id})
                            else:
                                raise ValidationError("Ya ha sido reservado completamente el producto " + line.line_id.name)
                




































            elif len(f2)>0:
                cant = 0
                for item in f2:
                    print ("Esto es item",item)
                    cant = cant + item.qty
                print ("cant in F2", cant)
                print ("existencias",existencias)
                existencias_no_reservadas = existencias - cant
                print ("existencias no reservadas",existencias_no_reservadas)

                if existencias_no_reservadas <= 0:
                    state = 'waiting'
                    if line.qty > 0.0:
                        order_line_reserve_qty = line.line_id.reserved_qty
                        order_line_reserve_qty += line.qty
                        if 1==1:
                            quote_id = quote_obj.create({
                                'order_id': line.order_id.id,
                                'line_id': line.line_id.id,
                                'product_id': line.product_id.id,
                                'product_uom': line.product_uom.id,
                                'name': line.line_id.name,
                                'qty': line.qty,
                                'state': state
                                })
                            if quote_id:
                                print ("la orden ", line.line_id.order_id)
                                line.line_id.order_id.write({'is_stock_reserv_created': True})
                                if state=='draft':
                                    line.line_id.write({'is_reserved': True, 'reserved_qty': order_line_reserve_qty, 'quote_id': quote_id.id})
                                else:
                                    line.line_id.write({'quote_id': quote_id.id})
                        else:
                            raise ValidationError("Ya ha sido reservado completamente el producto " + line.line_id.name)
                    else:
                        raise ValidationError("La cantidad a reservar del producto "+line.line_id.name+" debe ser mayor a 0")

                else:

                    if line.qty > 0.0:
                        if line.qty <= existencias_no_reservadas:
                            state = 'draft'
                            if line.qty > 0.0:
                                order_line_reserve_qty = line.line_id.reserved_qty
                                order_line_reserve_qty += line.qty
                                if 1==1:
                                    quote_id = quote_obj.create({
                                        'order_id': line.order_id.id,
                                        'line_id': line.line_id.id,
                                        'product_id': line.product_id.id,
                                        'product_uom': line.product_uom.id,
                                        'name': line.line_id.name,
                                        'qty': line.qty,
                                        'state': state
                                        })
                                    if quote_id:
                                        print ("la orden ", line.line_id.order_id)
                                        line.line_id.order_id.write({'is_stock_reserv_created': True})
                                        if state=='draft':
                                            line.line_id.write({'is_reserved': True, 'reserved_qty': order_line_reserve_qty, 'quote_id': quote_id.id})
                                        else:
                                            line.line_id.write({'quote_id': quote_id.id})
                                else:
                                    raise ValidationError("Ya ha sido reservado completamente el producto " + line.line_id.name)
                            else:
                                raise ValidationError("La cantidad a reservar del producto "+line.line_id.name+" debe ser mayor a 0")

                        else:
                            state = 'waiting'

                            canti = line.qty - existencias_no_reservadas

                            order_line_reserve_qty = line.line_id.reserved_qty
                            order_line_reserve_qty += line.qty
                            if 1==1:
                                quote_id = quote_obj.create({
                                    'order_id': line.order_id.id,
                                    'line_id': line.line_id.id,
                                    'product_id': line.product_id.id,
                                    'product_uom': line.product_uom.id,
                                    'name': line.line_id.name,
                                    'qty': canti,
                                    'state': state
                                    })
                                if quote_id:
                                    print ("la orden ", line.line_id.order_id)
                                    line.line_id.order_id.write({'is_stock_reserv_created': True})
                                    if state=='draft':
                                        line.line_id.write({'is_reserved': True, 'reserved_qty': order_line_reserve_qty, 'quote_id': quote_id.id})
                                    else:
                                        line.line_id.write({'quote_id': quote_id.id})
                            else:
                                raise ValidationError("Ya ha sido reservado completamente el producto " + line.line_id.name)

                            state = 'draft'
                            order_line_reserve_qty = line.line_id.reserved_qty
                            order_line_reserve_qty += line.qty
                            if 1==1:
                                quote_id = quote_obj.create({
                                    'order_id': line.order_id.id,
                                    'line_id': line.line_id.id,
                                    'product_id': line.product_id.id,
                                    'product_uom': line.product_uom.id,
                                    'name': line.line_id.name,
                                    'qty': existencias_no_reservadas,
                                    'state': state
                                    })
                                if quote_id:
                                    print ("la orden ", line.line_id.order_id)
                                    line.line_id.order_id.write({'is_stock_reserv_created': True})
                                    if state=='draft':
                                        line.line_id.write({'is_reserved': True, 'reserved_qty': order_line_reserve_qty, 'quote_id': quote_id.id})
                                    else:
                                        line.line_id.write({'quote_id': quote_id.id})
                            else:
                                raise ValidationError("Ya ha sido reservado completamente el producto " + line.line_id.name)
                

























            else:
                if line.qty > 0.0:
                    
                    if line.qty <= existencias:
                        state = 'draft'
                        order_line_reserve_qty = line.line_id.reserved_qty
                        order_line_reserve_qty += line.qty
                        if 1==1:
                            quote_id = quote_obj.create({
                                'order_id': line.sale_order_id.id,
                                'line_id': line.order_line_id.id,
                                'product_id': line.product_id.id,
                                'product_uom': line.uom_id.id,
                                'name': line.order_line_id.name,
                                'qty': line.qty,
                                'state': state
                                })
                            if quote_id:
                                print ("la orden ", line.line_id.order_id)
                                line.line_id.order_id.write({'is_stock_reserv_created': True})
                                if state=='draft':
                                    line.line_id.write({'is_reserved': True, 'reserved_qty': order_line_reserve_qty, 'quote_id': quote_id.id})
                                else:
                                    line.line_id.write({'quote_id': quote_id.id})
                        else:
                            raise ValidationError("Ya ha sido reservado completamente el producto " + line.line_id.name)
                    else:
                        state = 'waiting'

                        canti = line.qty - existencias

                        order_line_reserve_qty = line.line_id.reserved_qty
                        order_line_reserve_qty += line.qty
                        if 1==1:
                            quote_id = quote_obj.create({
                                'order_id': line.order_id.id,
                                'line_id': line.line_id.id,
                                'product_id': line.product_id.id,
                                'product_uom': line.product_uom.id,
                                'name': line.line_id.name,
                                'qty': canti,
                                'state': state
                                })
                            if quote_id:
                                print ("la orden ", line.line_id.order_id)
                                line.line_id.order_id.write({'is_stock_reserv_created': True})
                                if state=='draft':
                                    line.line_id.write({'is_reserved': True, 'reserved_qty': order_line_reserve_qty, 'quote_id': quote_id.id})
                                else:
                                    line.line_id.write({'quote_id': quote_id.id})
                        else:
                            raise ValidationError("Ya ha sido reservado completamente el producto " + line.line_id.name)

                        state = 'draft'
                        order_line_reserve_qty = line.line_id.reserved_qty
                        order_line_reserve_qty += line.qty
                        if 1==1:
                            quote_id = quote_obj.create({
                                'order_id': line.order_id.id,
                                'line_id': line.line_id.id,
                                'product_id': line.product_id.id,
                                'product_uom': line.product_uom.id,
                                'name': line.line_id.name,
                                'qty': existencias,
                                'state': state
                                })
                            if quote_id:
                                print ("la orden ", line.line_id.order_id)
                                line.line_id.order_id.write({'is_stock_reserv_created': True})
                                if state=='draft':
                                    line.line_id.write({'is_reserved': True, 'reserved_qty': order_line_reserve_qty, 'quote_id': quote_id.id})
                                else:
                                    line.line_id.write({'quote_id': quote_id.id})
                        else:
                            raise ValidationError("Ya ha sido reservado completamente el producto " + line.line_id.name)
                else:
                    raise ValidationError("La cantidad a reservar del producto "+line.line_id.name+" debe ser mayor a 0")

            line.unlink()
























































    @api.multi
    def action_create_reservation_(self):
        self.ensure_one()
        quote_obj = self.env['sale.order.quote']
        for line in self.reservation_line_ids:
            print ("cantidad disponible", line.product_id.virtual_available)
            if line.stock_reservation_qty > line.product_id.virtual_available:
                raise ValidationError("Solo hay " + str(line.product_id.virtual_available) + " existencias del producto: "+ line.order_line_id.name)
            else:
                quote_ids = self.env['sale.order.quote'].search([
                    ('product_id','=',line.product_id.id),
                    ('state','=','draft')
                    ])
                print ("quote_ids ", quote_ids)
                for quote in quote_ids:
                    print ("quote ", quote.start_date)
                    if not quote.start_date:
                        continue
                    print ("quote ", quote.d1)
                    print ("quote ", quote.d2)
                    print ("line ", line.order_line_id.start_date)
                    print ("line ", line.order_line_id.end_date)
                    #=ambos Entre=
                    #f1 >= F1 and f1 <= F2
                    if (line.order_line_id.d1 >= quote.d1) and (line.order_line_id.d1 <=quote.d2):
                        raise ValidationError("1) Ya se encuentra reservado el producto en otra cotizacion")
                    #=solo f1 entre=
                    #f1 >= F1 and f1 <= F2 and f2 > F2
                    if (line.order_line_id.d1 >= quote.d1) and (line.order_line_id.d1 <= quote.d2) and (line.order_line_id.d2 > quote.d2):
                        raise ValidationError("2) Ya se encuentra reservado el producto en otra cotizacion")
                    #=solo f2 entre=
                    #f2 <= F2 and f2 >= F1 and f1 < F1
                    if (line.order_line_id.d2 <= quote.d2) and (line.order_line_id.d2 >= quote.d1) and (line.order_line_id.d1 < quote.d2):
                        raise ValidationError("3) Ya se encuentra reservado el producto en otra cotizacion")
                #if quote_ids:
                    #raise ValidationError("Ya se encuentra reservado el producto en otra cotizacion")
                else:
                    if line.stock_reservation_qty > 0.0:
                        order_line_reserve_qty = line.order_line_id.reserved_qty
                        order_line_reserve_qty += line.stock_reservation_qty
                        if 1==1:
                            quote_id = quote_obj.create({
                                'order_id': line.sale_order_id.id,
                                'line_id': line.order_line_id.id,
                                'product_id': line.product_id.id,
                                'product_uom': line.uom_id.id,
                                'name': line.order_line_id.name,
                                'qty': line.stock_reservation_qty,
                                'state': 'draft'
                                })
                            if quote_id:
                                print ("la orden ", line.order_line_id.order_id)
                                line.order_line_id.order_id.write({'is_stock_reserv_created': True})
                                line.order_line_id.write({'is_reserved': True, 'reserved_qty': order_line_reserve_qty, 'quote_id': quote_id.id})
                        else:
                            raise ValidationError("Ya ha sido reservado completamente el producto " + line.order_line_id.name)
                    else:
                        raise ValidationError("La cantidad a reservar debe ser mayor a 0")


class StockReservationLine(models.TransientModel):
    _name = "sale.reservation.line"

    reservation_id = fields.Many2one(
        'sale.reservation',
        string='Reservation',
    )
    sale_order_id = fields.Many2one(
        'sale.order',
        string='Cotizacion',
        related="reservation_id.sale_order_id",
        store=True,
    )
    order_line_id = fields.Many2one(
        'sale.order.line',
        string='Linea del pedido',
        required=True,
        ondelete='cascade',

    )
    product_id = fields.Many2one(
        'product.product',
        string='Producto',
    )
    product_qty = fields.Float(
        string="Cantidad",
    )
    stock_reservation_qty = fields.Float(
        string="Cantidad a reservar",
        required=True,
    )
    uom_id = fields.Many2one(
#        'product.uom',
        'uom.uom',
        string="Unidad",
    )

    @api.onchange("order_line_id")
    def _onchange_order_line_id(self):
        self.product_id = self.order_line_id.product_id.id
        self.product_qty = self.order_line_id.product_uom_qty
        self.stock_reservation_qty = self.order_line_id.product_uom_qty
        self.uom_id = self.order_line_id.product_uom.id

    @api.onchange("stock_reservation_qty")
    def _onchange_stock_reservation_qty(self):
        if self.stock_reservation_qty > self.product_qty:
            raise ValidationError('La cantidad a reservar no puede ser mayor a la cantidad cotizada')

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
