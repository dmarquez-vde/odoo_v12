
# -*- coding: utf-8 -*-
# ##############################################################################
#

#    OpenERP, Open Source Management Solution
#    Copyright (C) 2012-Today Acespritech Solutions Pvt Ltd
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

class SalePo(models.TransientModel):
    _name = "sale.po"

    partner_id = fields.Many2one('res.partner',string='Partner')
    faltantes_line_ids = fields.One2many(
        'sale.po.line',
        'faltante_id',
        string='Lineas pedido',
    )
    sale_order_id = fields.Many2one(
        'sale.order',
        string='Cotizacion',
        readonly=True,
    )
    mismo = fields.Boolean(store=True, default=True)
    referencia = fields.Char (string="Referencia")



    @api.model
    def default_get(self,fields):
        res = super(SalePo, self).default_get(fields)
        vals = []
        order_id = self._context.get('active_id')
        print ("ids ", order_id)
        lines = self.env['sale.order.quote'].search([('order_id', '=', order_id)])
        print ("lines ", lines)
        for line in lines:
            if line.state == 'waiting':
                print ("line state", line.state)
                vals.append((0, 0, {
                    'order_id': order_id,
                    'order_line_id': line.id,
                    'product_id': line.product_id.id,
                    'product_qty': line.qty,
                    }))

        print ("vals ", vals)
        if len(vals) < 1:
            raise ValidationError("No hay productos para añadir a la orden de compra")
        res.update({'faltantes_line_ids': vals})
        return res
    


    @api.onchange('partner_id')
    def _get_partner_id(self):
        self.ensure_one()
        print ("Esto es self en onchange partner_id",self)
        print ("Esto es self.partner",self.partner_id)
        print ("esto es self.faltantes_line_ids", self.faltantes_line_ids)
        for line in self.faltantes_line_ids:
            print ("Entro al if esto es line",line)
            line.partner_line_id = self.partner_id

    @api.onchange('mismo')
    def _get_mismo_line(self):
        self.ensure_one()
        print ("Esto es mismo",self.mismo)
        print ("esto es self.faltantes_line_ids", self.faltantes_line_ids)
        for line in self.faltantes_line_ids:
            print ("Entro al if esto es line",line)
            line.mismo_line = self.mismo 

    @api.multi
    def action_create_po_faltante(self):
        self.ensure_one()
        print("Aqui solo hace un print")
        purchase_order = self.env['purchase.order']
        purchase_order_line = self.env['purchase.order.line']
        account_tax_obj = self.env['account.tax']
        registros = self.faltantes_line_ids
        order_id = self._context.get('active_id')
        mismo_proveedor = []
        vals = []

        if self.mismo == False:
            print ("se paso al if cuando es diferente Provedor ")
            for algo in self.faltantes_line_ids:
                if algo.partner_line_id.id not in mismo_proveedor:
                    mismo_proveedor.append(algo.partner_line_id.id)
            print ("Esto es mismo_proveedor",mismo_proveedor)


            for algo1 in mismo_proveedor:
                print("ESTOS SON LOS REGISTROS antes",registros)
                registros1 = registros.search([('partner_line_id', '=', algo1),('order_id', '=', order_id)])
                print("ESTOS SON LOS REGISTROS despues",registros1)
                orden = {
                'partner_id': algo1,
                'sale_related': order_id,
                'partner_ref':self.referencia,
                }
                orden_id = purchase_order.create(orden)
                print("Esto es vals antes",vals)
                for algo2 in registros1:
                    print("ESTA EN FOR ALGO2",algo2)
                    impuesto = account_tax_obj.search([('id', '=', algo2.taxes_ids.id)])
                    impuesto1 = []
                    impuesto1.append(impuesto.id)
                    print("ESTA EN FOR impuesto",algo2)
                    if impuesto1:
                        vals = {
                            'order_id':orden_id.id,
                            'product_id': algo.product_id.id,
                            'name': algo.product_id.name,
                            'price_unit':algo.costo,
                            'date_planned': algo.date_id,
                            'product_qty': algo.product_qty,
                            'product_uom': algo.product_id.uom_id.id,
                            'taxes_id': [(6, 0, impuesto1)],
                            }
                    else:
                        vals = {
                            'order_id':orden_id.id,
                            'product_id': algo.product_id.id,
                            'name': algo.product_id.name,
                            'price_unit':algo.costo,
                            'date_planned': algo.date_id,
                            'product_qty': algo.product_qty,
                            'product_uom': algo.product_id.uom_id.id,
                            }
                    print ("ESTO ES VALS",vals)
                    orden_line_id = purchase_order_line.create(vals)
                    vals = []
            view = self.env.ref('purchase.purchase_order_form')
            return {
                'name': _('purchase order'),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'purchase.order',
                'views': [(view.id, 'form')],
                'view_id': view.id,
                'target': 'current',
                'res_id': orden_id.id,
                'context': self.env.context,
            }


        else:
            print ("se paso al else cuando es mismo Provedor ")
            orden = {
                'partner_id': self.partner_id.id,
                'sale_related': order_id,
                'partner_ref':self.referencia,
                }
            orden_id = purchase_order.create(orden)
            for algo in self.faltantes_line_ids:
                impuesto = account_tax_obj.search([('id', '=', algo.taxes_ids.id)])
                impuesto1 = []
                impuesto1.append(impuesto.id)
                if impuesto1:
                    vals = {
                        'order_id':orden_id.id,
                        'product_id': algo.product_id.id,
                        'name': algo.product_id.name,
                        'price_unit':algo.costo,
                        'date_planned': algo.date_id,
                        'product_qty': algo.product_qty,
                        'product_uom': algo.product_id.uom_id.id,
                        'taxes_id': [(6, 0, impuesto1)],
                        }
                else:
                    vals = {
                        'order_id':orden_id.id,
                        'product_id': algo.product_id.id,
                        'name': algo.product_id.name,
                        'price_unit':algo.costo,
                        'date_planned': algo.date_id,
                        'product_qty': algo.product_qty,
                        'product_uom': algo.product_id.uom_id.id,
                        }
                
                orden_line_id = purchase_order_line.create(vals)
                vals = []

            view = self.env.ref('purchase.purchase_order_form')
            return {
                'name': _('purchase order'),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'purchase.order',
                'views': [(view.id, 'form')],
                'view_id': view.id,
                'target': 'current',
                'res_id': orden_id.id,
                'context': self.env.context,
            }

    

class SalePoLine(models.TransientModel):
    _name = "sale.po.line"

    faltante_id = fields.Many2one('sale.po')

    taxes_ids = fields.Many2one('account.tax', string='Taxes')

    order_line_id = fields.Many2one(
	       'sale.order.quote',
	       string='Linea del pedido',
	       required=True,
	       ondelete='cascade',
	   )

    product_id = fields.Many2one(
	       'product.product',
	       string='Producto',
	   )

    product_qty = fields.Float(
	       string="Cantidad",
	   )

    costo = fields.Float(
	       string="Costo unitario",
	   )
    uom_id = fields.Many2one(
	       'uom.uom',
	       string="Unidad",
	   )

    date_id = fields.Datetime(
	       string="Fecha prevista",
	   )

    partner_line_id = fields.Many2one('res.partner',string='Provedor')

    order_id = fields.Many2one(
        'sale.order',
        string='Cotizacion',
        readonly=True,
    )

    mismo_line = fields.Boolean(store=True, default=True)