# -*- coding: utf-8 -*-
# ##############################################################################
#

#    OpenERP, Open Source Management Solution
#    Copyright (C) 2012-Today Acespritech Solutions Pvt Ltd
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import models, fields, api, _
from odoo.addons import decimal_precision as dp
from odoo.exceptions import ValidationError, UserError
from odoo.tools.float_utils import float_compare, float_is_zero, float_round
import datetime
import pytz

import logging
_logger = logging.getLogger(__name__)

class StockProductionLot(models.Model):

    _inherit = 'stock.production.lot'

    purchase = fields.Boolean(string="Para compra de proveedor?", default=False)

class ProcurementGroup(models.Model):

    _inherit = 'procurement.group'

    #def convert_to_utc(self, fecha):
        #to_zone = tz.gettz("UTC")
        #from_zone = tz.gettz("America/Mexico_City")
        #local = fecha.replace(tzinfo=from_zone)
        #utc = local.astimezone(to_zone)
        #return utc

    def cambio_fecha_hora_utc(self, fecha):
        timezone = pytz.timezone("America/Mexico_City")
        fecha_local = timezone.localize(fecha, is_dst=None)
        fecha_utc = fecha_local.astimezone(pytz.utc)
        return fecha_utc

    @api.model
    def run(self, product_id, product_qty, product_uom, location_id, name, origin, values):
        """ Method used in a procurement case. The purpose is to supply the
        product passed as argument in the location also given as an argument.
        In order to be able to find a suitable location that provide the product
        it will search among stock.rule.
        """
        line = self.env['sale.order.line'].browse(values.get('sale_line_id'))
        if line.order_id.is_rental == True:
            planned_local = line.d1
            planned_utc = self.cambio_fecha_hora_utc(datetime.datetime(planned_local.year,planned_local.month,planned_local.day,9,00,00))
            planned_utc = datetime.datetime(planned_utc.year,planned_utc.month,planned_utc.day,planned_utc.hour,planned_utc.minute,planned_utc.second)
            values.setdefault('company_id', self.env['res.company']._company_default_get('procurement.group'))
            values.setdefault('priority', '1')
            values.setdefault('date_planned', planned_utc)
            values.update({'date_planned':planned_utc})
            rule = self._get_rule(product_id, location_id, values)
            if not rule:
                raise UserError(_('No procurement rule found in location "%s" for product "%s".\n Check routes configuration.') % (location_id.display_name, product_id.display_name))
            action = 'pull' if rule.action == 'pull_push' else rule.action
            if hasattr(rule, '_run_%s' % action):
                getattr(rule, '_run_%s' % action)(product_id, product_qty, product_uom, location_id, name, origin, values)
            else:
                _logger.error("The method _run_%s doesn't exist on the procument rules" % action)
            return True
        else:
            values.setdefault('company_id', self.env['res.company']._company_default_get('procurement.group'))
            values.setdefault('priority', '1')
            values.setdefault('date_planned', fields.Datetime.now())
            rule = self._get_rule(product_id, location_id, values)
            if not rule:
                raise UserError(_('No procurement rule found in location "%s" for product "%s".\n Check routes configuration.') % (location_id.display_name, product_id.display_name))
            action = 'pull' if rule.action == 'pull_push' else rule.action
            if hasattr(rule, '_run_%s' % action):
                getattr(rule, '_run_%s' % action)(product_id, product_qty, product_uom, location_id, name, origin, values)
            else:
                _logger.error("The method _run_%s doesn't exist on the procument rules" % action)
            return True



    @api.model
    def _get_rule(self, product_id, location_id, values):
        """ Find a pull rule for the location_id, fallback on the parent
        locations if it could not be found.
        """
        #print ("values ", values)
        result = False
        domain = []
        location = location_id
        line = self.env['sale.order.line'].browse(values.get('sale_line_id'))
        if line.is_rental:
            location_src = line.product_id.property_stock_outgoing.id
            domain = [('location_id', '=', location.id),('action', '!=', 'push'),('location_src_id','=',location_src)]
        else:
            domain = [('location_id', '=', location.id), ('action', '!=', 'push')]
        print ("dominio ", domain)
        while (not result) and location:
            result = self._search_rule(values.get('route_ids', False), product_id, values.get('warehouse_id', False), domain)
            location = location.location_id
        print ("el result ", result)
        return result


class StockMove(models.Model):

    _inherit = 'stock.move'

    quote_id = fields.Many2one('sale.order.quote', related="sale_line_id.quote_id", string="Reservacion")


class stockPicking(models.Model):
    
    _inherit="stock.picking"

    state = fields.Selection([
        ('draft', 'Draft'),
        ('waiting', 'Waiting Another Operation'),
        ('confirmed', 'Waiting'),
        ('assigned', 'Ready'),
        ('done', 'Done'),
        ('returned', 'Devolución preparada'),
        ('almacen', 'Devuelto a almacen'),
        ('cancel', 'Cancelled'),
    ], string='Status', compute='_compute_state',
        copy=False, index=True, readonly=True, store=True, track_visibility='onchange',
        help=" * Draft: not confirmed yet and will not be scheduled until confirmed.\n"
             " * Waiting Another Operation: waiting for another move to proceed before it becomes automatically available (e.g. in Make-To-Order flows).\n"
             " * Waiting: if it is not ready to be sent because the required products could not be reserved.\n"
             " * Ready: products are reserved and ready to be sent. If the shipping policy is 'As soon as possible' this happens as soon as anything is reserved.\n"
             " * Done: has been processed, can't be modified or cancelled anymore.\n"
             " * Cancelled: has been cancelled, can't be confirmed anymore.\n"
             " * returned: has been returned, in rent.")

    @api.multi
    def action_assign(self):
        """ Check availability of picking moves.
        This has the effect of changing the state and reserve quants on available moves, and may
        also impact the state of the picking as it is computed based on move's states.
        @return: True
        """
        self.filtered(lambda picking: picking.state == 'draft').action_confirm()
        moves = self.mapped('move_lines').filtered(lambda move: move.state not in ('draft', 'cancel', 'done'))
        if not moves:
            raise UserError(_('Nothing to check the availability for.'))
        # If a package level is done when confirmed its location can be different than where it will be reserved.
        # So we remove the move lines created when confirmed to set quantity done to the new reserved ones.
        package_level_done = self.mapped('package_level_ids').filtered(lambda pl: pl.is_done and pl.state == 'confirmed')
        package_level_done.write({'is_done': False})
        moves._action_assign()
        package_level_done.write({'is_done': True})

        quote_ids = self.env['sale.order.quote'].search([
                ('order_id','=',self.sale_id.id),
                ('state','in',['draft','partial_available']),
                ('start_date','!=',False),
                ('end_date','!=',False),
                ])

        for line in quote_ids:
            line.state = 'assigned'

        
        return True


    @api.multi
    def button_validate(self):

        name = str(self.name)
        name1 = name[0:6]
        print ("Esto es name1",name1)
        self.ensure_one()
        if not self.move_lines and not self.move_line_ids:
            raise UserError(_('Please add some items to move.'))
        print ("ENTRA AQUI 1")
        # If no lots when needed, raise error
        picking_type = self.picking_type_id
        precision_digits = self.env['decimal.precision'].precision_get('Product Unit of Measure')
        no_quantities_done = all(float_is_zero(move_line.qty_done, precision_digits=precision_digits) for move_line in self.move_line_ids)
        no_reserved_quantities = all(float_is_zero(move_line.product_qty, precision_rounding=move_line.product_uom_id.rounding) for move_line in self.move_line_ids)
        if no_reserved_quantities and no_quantities_done:
            print ("ENTRA AQUI 2")
            raise UserError(_('Primero debe dar click en el boton comprobar disponibilidad, No puede validar una transferencia si no se reservan o agregan las cantidades. Para forzar la transferencia, cambie editar y modifique las cantidades hechas.'))

        if picking_type.use_create_lots or picking_type.use_existing_lots:
            lines_to_check = self.move_line_ids
            print ("ENTRA AQUI 3")
            if not no_quantities_done:
                print ("ENTRA AQUI 4")
                lines_to_check = lines_to_check.filtered(
                    lambda line: float_compare(line.qty_done, 0,
                                               precision_rounding=line.product_uom_id.rounding)
                )

            for line in lines_to_check:
                product = line.product_id
                if product and product.tracking != 'none':
                    print ("ENTRA AQUI 5")
                    if not line.lot_name and not line.lot_id:
                        print ("ENTRA AQUI 6")
                        raise UserError(_('Debe proporcionar un número de lote / serie para el producto %s.') % product.display_name)


        if no_quantities_done:
            print ("ENTRA AQUI 7")

            view = self.env.ref('stock.view_immediate_transfer')
            wiz = self.env['stock.immediate.transfer'].create({'pick_ids': [(4, self.id)]})
            

            return {
                'name': _('Immediate Transfer?'),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'stock.immediate.transfer',
                'views': [(view.id, 'form')],
                'view_id': view.id,
                'target': 'new',
                'res_id': wiz.id,
                'context': self.env.context,
            }

        if self._get_overprocessed_stock_moves() and not self._context.get('skip_overprocessed_check'):
            print ("ENTRA AQUI 8")
            view = self.env.ref('stock.view_overprocessed_transfer')
            wiz = self.env['stock.overprocessed.transfer'].create({'picking_id': self.id})
            return {
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'stock.overprocessed.transfer',
                'views': [(view.id, 'form')],
                'view_id': view.id,
                'target': 'new',
                'res_id': wiz.id,
                'context': self.env.context,
            }

        # Check backorder should check for other barcodes
        if self._check_backorder():
            print ("ENTRA AQUI 9")
            return self.action_generate_backorder_wizard()


        
        if name1 == "WH/OUT":
            quote_ids = self.env['sale.order.quote'].search([
                ('order_id','=',self.sale_id.id),
                ('state','in',['draft','assigned']),
                ('start_date','!=',False),
                ('end_date','!=',False),
                ])

            for line in quote_ids:
                line.state = 'done'
            print ("ENTRA AQUI 10")

        elif name1 == "WH/IN/":

            quote_ids = self.env['sale.order.quote'].search([
                ('order_id','=',self.sale_id.id),
                ('state','=','waiting_for_return'),
                ('start_date','!=',False),
                ('end_date','!=',False),
                ])

            for line in quote_ids:
                line.state = 'returned'
            print ("ENTRA AQUI 13")

            if name1 == "WH/IN/":
                print ("ENTRO HASTA AQUI 2")
                self.state = 'almacen'

        self.action_done()

        if name1 == "WH/IN/":
            print ("ENTRO HASTA AQUI 21")
            self.state = 'almacen'
        
        return

    @api.depends('move_type', 'move_lines.state', 'move_lines.picking_id')
    @api.one
    def _compute_state(self):
        
        ''' State of a picking depends on the state of its related stock.move
        - Draft: only used for "planned pickings"
        - Waiting: if the picking is not ready to be sent so if
          - (a) no quantity could be reserved at all or if
          - (b) some quantities could be reserved and the shipping policy is "deliver all at once"
        - Waiting another move: if the picking is waiting for another move
        - Ready: if the picking is ready to be sent so if:
          - (a) all quantities are reserved or if
          - (b) some quantities could be reserved and the shipping policy is "as soon as possible"
        - Done: if the picking is done.
        - Cancelled: if the picking is cancelled
        '''
        if not self.move_lines:
            self.state = 'draft'
        elif any(move.state == 'draft' for move in self.move_lines):  # TDE FIXME: should be all ?
            self.state = 'draft'
        elif all(move.state == 'cancel' for move in self.move_lines):
            self.state = 'cancel'
        elif all(move.state in ['cancel', 'done'] for move in self.move_lines):
            self.state = 'done'
        else:
            relevant_move_state = self.move_lines._get_relevant_state_among_moves()
            if relevant_move_state == 'partially_available':
                self.state = 'assigned'
            else:
                self.state = relevant_move_state

        algo = self.env['stock.picking'].search([('id','=',self.id),('state','!=','cancel')])
        print ("Se encuentra aqui",algo)
        print ("Se encuentra aqui1",algo.picking_type_id.id)
        print ("Se encuentra aqui",algo.state)
        if algo.picking_type_id.id == 4 and algo.state=='done':
            algo.state = 'almacen'





    @api.multi
    def do_unreserve(self):
        for picking in self:
            picking.move_lines._do_unreserve()
            picking.package_level_ids.filtered(lambda p: not p.move_ids).unlink()

        print ("ENTRA AQUI y QUITA RESERVACION")
        quote_ids = self.env['sale.order.quote'].search([
                ('order_id','=',self.sale_id.id),
                ('state','=','assigned'),
                ('start_date','!=',False),
                ('end_date','!=',False),
                ])

        for line in quote_ids:
            line.state = 'draft'
        print ("ENTRA AQUI 10")
        self.action_done()
        return


class ReturnPicking(models.TransientModel):
    _inherit = 'stock.return.picking'

    def create_returns(self):
        print ("Esto es self in create_returns",self.env.context.get('active_id'))
        algo = self.env.context.get('active_id')
        algo1 = self.env['stock.picking'].search([('id','=',algo)])

        quote_ids = self.env['sale.order.quote'].search([
                ('order_id','=',algo1.sale_id.id),
                ('state','=','done'),
                ('start_date','!=',False),
                ('end_date','!=',False),
                ])

        for line in quote_ids:
            line.state = 'waiting_for_return'
        print ("ENTRA AQUI 11")

        for wizard in self:
            new_picking_id, pick_type_id = wizard._create_returns()
        # Override the context to disable all the potential filters that could have been set previously
        ctx = dict(self.env.context)
        ctx.update({
            'search_default_picking_type_id': pick_type_id,
            'search_default_draft': False,
            'search_default_assigned': False,
            'search_default_confirmed': False,
            'search_default_ready': False,
            'search_default_late': False,
            'search_default_available': False,
        })

        for retorna in algo1:
            retorna.state = 'returned'


        return {
            'name': _('Returned Picking'),
            'view_type': 'form',
            'view_mode': 'form,tree,calendar',
            'res_model': 'stock.picking',
            'res_id': new_picking_id,
            'type': 'ir.actions.act_window',
            'context': ctx,
        }



class StockImmediateTransfer(models.TransientModel):
    _inherit = 'stock.immediate.transfer'


    pick_to_backorder_ids = fields.Many2many('stock.picking', help='Picking to backorder')

    @api.multi
    def process(self):
        print ("Immediate transfer self",self)
        print ("Immediate transfer context",self.env.context)
        algo = self.env.context.get('active_ids')
        for algo2 in algo:
            algo2 = algo2
            print ("Esto es algo2",algo2)
        algo1 = self.env['stock.picking'].search([('sale_id','=',algo2),('state','!=','cancel')])
        print ("Esto es algo1 1",algo1)

        if not algo1:
            algo2 = algo2 + 1
            algo1 = self.env['stock.picking'].search([('id','=',algo2),('state','!=','cancel')])
            print ("Esto es algo1 2",algo1)


        elif len(algo1)>1:
            algo1 = self.env['stock.picking'].search([('sale_id','=',algo2),('picking_type_id','=',4),('state','!=','cancel')])
            print ("Esto es algo1 3",algo1)

        print ("Esto es algo1 original",algo1)
        name = str(algo1.name)
        name1 = name[0:6]
        print ("Esto es name1",name1)

        if name1 == "WH/OUT":
                quote_ids = self.env['sale.order.quote'].search([
                    ('order_id','=',algo1.sale_id.id),
                    ('state','in',['draft','assigned']),
                    ('start_date','!=',False),
                    ('end_date','!=',False),
                    ])

                for line in quote_ids:
                    line.state = 'done'
                print ("ENTRA AQUI 10")

        elif name1 == "WH/IN/":

            quote_ids = self.env['sale.order.quote'].search([
                ('order_id','=',algo1.sale_id.id),
                ('state','=','waiting_for_return'),
                ('start_date','!=',False),
                ('end_date','!=',False),
                ])

            for line in quote_ids:
                line.state = 'returned'
            print ("ENTRA AQUI algo1.state",algo1.state)
            algo1.state = 'almacen'
            print ("ENTRA AQUI 12 +++++++++++++")
            print ("ENTRA AQUI algo1.state despues de +++",algo1.state)

        backorder_wizard_dict = super(StockImmediateTransfer, self).process()
        # If the immediate transfer wizard process all our picking but with some back order maybe needed we want to add the backorder already passed to the wizard.
        if backorder_wizard_dict:
            print ("Esto es abajo en if")
            backorder_wizard = self.env['stock.backorder.confirmation'].browse(backorder_wizard_dict.get('res_id', False))
            backorder_wizard.write({'pick_ids': [(4, p.id) for p in self.pick_to_backorder_ids]})
            return backorder_wizard_dict
        # If there is no backorder returned by the immediate transfer basic function we still wanted to process those manually given
        elif self.pick_to_backorder_ids:
            print ("Esto es abajo en elif")
            return self.pick_to_backorder_ids.action_generate_backorder_wizard()
        return False

