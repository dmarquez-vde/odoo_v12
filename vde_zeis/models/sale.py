# -*- coding: utf-8 -*-
# ##############################################################################
#

#    OpenERP, Open Source Management Solution
#    Copyright (C) 2012-Today Acespritech Solutions Pvt Ltd
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import models, fields, api, _
from odoo.addons import decimal_precision as dp
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, float_compare
from odoo.exceptions import ValidationError, UserError
import datetime

class SaleOrderQuote(models.Model):

    _name ="sale.order.quote"

    order_id = fields.Many2one('sale.order', string="Orden")
    line_id = fields.Many2one('sale.order.line', string="Linea de pedido")
    sequence = fields.Integer(string='Sequence', default=10)
    product_id = fields.Many2one('product.product', string="Producto")
    product_uom = fields.Many2one('uom.uom', string='Unidad Medida')
    name = fields.Char(string="Descripcion")
    qty = fields.Float(string="Cantidad")
    d1 = fields.Date(string="D1", related="order_id.d1", store=True)
    start_date = fields.Date(string="Fecha inicio", related="order_id.start_date")
    d2 = fields.Date(string="D2", related="order_id.d2", store=True)
    end_date = fields.Date(string="Fecha fin", related="order_id.end_date")
    duration = fields.Float(string="Dias", related="order_id.duration", store=True)
    display_name = fields.Char(string="Display", compute="_get_display_name")
    #price_unit = fields.Float('Precio', required=True, digits=dp.get_precision('Product Price'), default=0.0)
    #price_subtotal = fields.Monetary(compute='_compute_amount', string='Subtotal', readonly=True, store=True)
    #price_tax = fields.Float(compute='_compute_amount', string='Impuestos', readonly=True, store=True)
    #price_total = fields.Monetary(compute='_compute_amount', string='Total', readonly=True, store=True)
    #tax_id = fields.Many2many('account.tax', string='Impuestos', domain=['|', ('active', '=', False), ('active', '=', True)])
    #discount = fields.Float(string='Discount (%)', digits=dp.get_precision('Discount'), default=0.0)
    #currency_id = fields.Many2one(related='order_id.currency_id', depends=['order_id'], store=True, string='Currency', readonly=True)
    #state = fields.Selection([
        #('prereserved', 'Pre-reservado'),
        #('reserved', 'Reservado'),
        #('done', 'Realizado'),
        #('cancel1', 'Cancelado'),
        #('cancel', 'Movimiento Cancelado'),
    #], string='Status', readonly=True, copy=False, store=True, default='prereserved')
    state = fields.Selection([
        ('draft', 'Pre-reservado'),
        ('cancel', 'Movimiento Cancelado'),
        ('cancel1', 'Pre-reserva cancelada'),
        ('waiting', 'Esperando otro movimiento'),
        ('confirmed', 'Esperando dispobilidad'),
        ('assigned', 'Reservado'),
        ('waiting_for_return', 'Esperando devolución'),
        ('returned', 'Devuelto a almacen'),
        ('done', 'Enviado, en renta'),
    ], string='Estado', copy=False)
    move_state = fields.Selection([
        ('draft', 'Nuevo'),
        ('cancel', 'Cancelado'),
        ('waiting', 'Esperando otro movimiento'),
        ('confirmed', 'Esperando dispobilidad'),
        ('partial_available', 'Parcialmente disponible'),
        ('assigned', 'Reservado'),
        ('done', 'Enviado, en renta'),
    ], string='Estado de movimiento', related="line_id.move_state")
    #], string='Estado de movimiento', compute="_get_move_state")

    @api.one
    @api.depends('line_id')
    def _get_move_state(self):
        move_id = self.env['stock.move'].search([('quote_id','=',self.id)])
        print ("move_id ", move_id)
        print ("move_id ", move_id.state)
        if move_id:
            self.move_state = move_id.state

    @api.one
    @api.depends('order_id', 'start_date', 'end_date')
    def _get_display_name(self):
        self.display_name = self.order_id.name + " del: " + str(self.start_date) + " al: " + str(self.end_date)




class SaleOrder(models.Model):

    _inherit = 'sale.order'

    is_rental = fields.Boolean(string="Es renta?", default=lambda self: self.env.context.get('renta'))
    start_date = fields.Date(string="Fecha inicio")
    end_date = fields.Date(string="Fecha fin")
    quote_ids = fields.One2many('sale.order.quote', 'order_id', string="Lineas de cotizacion")
    is_stock_reserv_created = fields.Boolean(
        string="Se creo reservacion?",
        copy=False,
        #default=False
        compute="_get_confirmation"
    )
    is_stock_reserv = fields.Boolean(
        string="Hay partidas creadas?",
        copy=False,
        default=False,
        compute="_get_confirmation"
    )

    reserved_ok = fields.Boolean(
        string="Ya hay reservaciones",
        copy=False,
        default=False,
        compute="_get_confirmation"
    )

    need_po = fields.Boolean(string="Necesita POs?", compute="_get_confirmation")
    quotes = fields.Boolean(string="Reservaciones creadas?", compute="_get_quotes")
    d1 = fields.Date(string="D1", compute="_get_dates")
    d2 = fields.Date(string="D2", compute="_get_dates")
    duration = fields.Float(string="Dias")

    @api.one
    @api.depends('quote_ids')
    def _get_quotes(self):
        if len(self.quote_ids) > 0:
            for quote in self.quote_ids:
                if quote.state != 'cancel1':
                    self.quotes = True
                    return True
                else:
                    self.quotes = False
        else:
            self.quotes = False

    @api.one
    @api.depends('quote_ids')
    def _get_confirmation(self):
        if len(self.quote_ids) > 0:

            for quote in self.quote_ids:
                if quote.state == 'cancel1':
                    continue
                else:
                    if quote.state == 'waiting':
                        self.is_stock_reserv = True
                        self.is_stock_reserv_created = True
                        self.need_po = True
                        return

                    else:
                        self.is_stock_reserv_created = True
                        self.is_stock_reserv = True
                        self.need_po = False
                        self.reserved_ok = True                  
        else:
            self.need_po = False
            self.reserved_ok = False

    @api.onchange('start_date','end_date')
    def onchange_dates(self):
        if self.start_date != False and self.end_date != False:
            if self.end_date < self.start_date:
                raise UserError("La fecha fin no puede ser menor a la fecha inicio")
            else:
                self.duration = (self.end_date - self.start_date).days

    @api.onchange('duration')
    def onchange_duration(self):
        if self.start_date  and self.end_date:
            self.duration = (self.end_date - self.start_date).days

    @api.one
    @api.depends('start_date', 'end_date')
    def _get_dates(self):
        if self.start_date != False and self.end_date != False:
            self.d2 = self.end_date + datetime.timedelta(days=2)
            self.d1 = self.start_date - datetime.timedelta(days=1)


    @api.multi
    def action_create_sale_reservation(self):
        self.ensure_one()
        return {
            'name': _('Reservacion de venta'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'sale.reservation',
            'view_id': self.env.ref('vde_zeis.wiz_sale_reservation_view').id,
            'type': 'ir.actions.act_window',
            'context': {'default_sale_order_id':self.id},
            'target': 'new'
        }

    @api.multi
    def action_reload_create_sale_reservation(self):
        self.ensure_one()
        return {
            'name': _('Reintentar Reservacion de venta'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'sale.reservation',
            'view_id': self.env.ref('vde_zeis.wiz_sale_reservation_view_reload').id,
            'type': 'ir.actions.act_window',
            'context': {'default_sale_order_id':self.id},
            'target': 'new'
        }

    @api.multi
    def action_create_po(self):
        self.ensure_one()

        return {
            'name': _('Crear pedido de renta'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'sale.po',
            'view_id': self.env.ref('vde_zeis.wiz_sale_create_po_view').id,
            'type': 'ir.actions.act_window',
            'context': {'default_sale_order_id':self.id},
            'target': 'new'
        }

    @api.multi
    def action_cancel_sale_reservation(self):
        self.ensure_one()
        for line in self.quote_ids:
            if (line.write({'state':'cancel1'})):
                if (line.line_id.write({'is_reserved':False, 'reserved_qty':0, 'quote_id':False})):
                    line.order_id.write({'is_stock_reserv_created':False})

    @api.multi
    def action_confirm(self):
        for order in self:
            if (order.start_date == False or order.end_date == False) and order.is_rental == True:
                raise ValidationError("No puede confirmar el pedido sin las Fechas de inicio y fin")
        return super(SaleOrder, self).action_confirm()


class SaleOrderLine(models.Model):

    _inherit = 'sale.order.line'

    is_rental = fields.Boolean(string="Es renta?", related="order_id.is_rental")
    is_reserved = fields.Boolean(string="Reservado?", default=False, copy=False)
    reserved_qty = fields.Float(string="Cantidad reservada", copy=False)
    start_date = fields.Date(string="Fecha inicio", related="order_id.start_date")
    end_date = fields.Date(string="Fecha fin", related="order_id.end_date")
    duration = fields.Float(string="Dias", related="order_id.duration", store=True)
    d1 = fields.Date(string="D1", related="order_id.d1", store=True)
    d2 = fields.Date(string="D2", related="order_id.d2", store=True)
    quote_id = fields.Many2one('sale.order.quote', string="Reservacion", copy=False)
    move_state = fields.Selection([
        ('draft', 'Nuevo'),
        ('cancel', 'Cancelado'),
        ('waiting', 'Esperando otro movimiento'),
        ('confirmed', 'Esperando dispobilidad'),
        ('partial_available', 'Parcialmente disponible'),
        ('assigned', 'Reservado'),
        ('done', 'Hecho'),
    ], string='Estado de movimiento', compute='_get_move_state')


    @api.onchange('product_uom_qty', 'product_uom', 'route_id')
    def _onchange_product_id_check_availability(self):
        if not self.product_id or not self.product_uom_qty or not self.product_uom:
            self.product_packaging = False
            return {}
        if self.product_id.type == 'product':
            precision = self.env['decimal.precision'].precision_get('Product Unit of Measure')
            product = self.product_id.with_context(
                warehouse=self.order_id.warehouse_id.id,
                lang=self.order_id.partner_id.lang or self.env.user.lang or 'en_US'
            )
            product_qty = self.product_uom._compute_quantity(self.product_uom_qty, self.product_id.uom_id)
            if float_compare(product.virtual_available, product_qty, precision_digits=precision) == -1:
                is_available = self._check_routing()
                if not is_available:
                    message =  _('Tu planeas vender %s %s de %s pero solamente tienes %s %s disponibles en  %s almacen.') % \
                            (self.product_uom_qty, self.product_uom.name, self.product_id.name, product.virtual_available, product.uom_id.name, self.order_id.warehouse_id.name)
                    # We check if some products are available in other warehouses.
                    if float_compare(product.virtual_available, self.product_id.virtual_available, precision_digits=precision) == -1:
                        message += _('\nThere are %s %s available across all warehouses.\n\n') % \
                                (self.product_id.virtual_available, product.uom_id.name)
                        for warehouse in self.env['stock.warehouse'].search([]):
                            quantity = self.product_id.with_context(warehouse=warehouse.id).virtual_available
                            if quantity > 0:
                                message += "%s: %s %s\n" % (warehouse.name, quantity, self.product_id.uom_id.name)
                    warning_mess = {
                        'title': _('Not enough inventory!'),
                        'message' : message
                    }
                    return {'warning': warning_mess}
        return {}

    @api.one
    @api.depends('move_ids')
    def _get_move_state(self):
        for move in self.move_ids:
            if move.sale_line_id.id == self.id:
                self.move_state = move.state


    @api.one
    @api.depends('move_ids')
    def _get_move_state_(self):
        moves = len(self.move_ids)
        cont = 0
        #print ("moves ", moves)
        for move in self.move_ids:
            line = move.sale_line_id
            if move.state=='cancel':
                cont = cont + 1
        #print ("cont ", cont)
        #print ("line ", line)
        #print ("line.quote_id ", line.quote_id)
        if moves > 0:
            if cont == moves:
                line.quote_id.write({'state':'cancel'})
                return True
            for move in self.move_ids:
                if move.state=='cancel':
                    continue
                if move.sale_line_id.id == self.id:
                    print ("quote_id ", move.sale_line_id.quote_id)
                    if move.sale_line_id.quote_id.state != 'cancel1':
                        self.move_state = move.state
                    print ("estado ", move.sale_line_id.quote_id.state)
                    if move.sale_line_id.quote_id.state != 'cancel1':
                        print ("va a escribir")
                        if move.state == 'cancel':
                            print ("cancel")
                            #move.sale_line_id.quote_id.state = 'cancel'
                            move.sale_line_id.quote_id.write({'state':'cancel'})
                        if move.state == 'waiting':
                            print ("waiting")
                            #move.sale_line_id.quote_id.state = 'waiting'
                            move.sale_line_id.quote_id.write({'state':'waiting'})
                        if move.state == 'confirmed':
                            print ("confirmed")
                            #move.sale_line_id.quote_id.state = 'confirmed'
                            move.sale_line_id.quote_id.write({'state':'confirmed'})
                        if move.state == 'assigned':
                            print ("assigned")
                            #move.sale_line_id.quote_id.state = 'assigned'
                            move.sale_line_id.quote_id.write({'state':'assigned'})
                        if move.state == 'done':
                            print ("done")
                            #move.sale_line_id.quote_id.state = 'done'
                            move.sale_line_id.quote_id.write({'state':'done'})
                    #if move.state != 'cancel':
                        #print ("move.sale_line_id.id ", move.sale_line_id)
                        ##if move.state != 'cancel1':
                        #self.move_state = move.state
                        #print ("quote_id ", self.quote_id)
                        #print ("quote_id ", self.quote_id.state)
                        #print ("move.state ", move.state)
                        #print ("mmove.sale_line_id.state ", move.sale_line_id.state)
                        #if move.sale_line_id.quote_id:
                            #if move.sale_line_id.state != 'cancel':
                                #if move.state == 'cancel':
                                    #move.sale_line_id.move_state = 'cancel'
                                #if move.state == 'waiting':
                                    #move.sale_line_id.move_state = 'waiting'
                                #if move.state == 'confirmed':
                                    #move.sale_line_id.move_state = 'confirmed'
                                #if move.state == 'assigned':
                                    #move.sale_line_id.move_state = 'assigned'
                                #if move.state == 'done':
                                    #move.sale_line_id.move_state = 'done'

    @api.multi
    def _action_launch_stock_rule(self):
        """
        Launch procurement group run method with required/custom fields genrated by a
        sale order line. procurement group will launch '_run_pull', '_run_buy' or '_run_manufacture'
        depending on the sale order line product rule.
        """
        precision = self.env['decimal.precision'].precision_get('Product Unit of Measure')
        errors = []
        for line in self:
            if line.state != 'sale' or not line.product_id.type in ('consu','product'):
                continue
            qty = line._get_qty_procurement()
            if float_compare(qty, line.product_uom_qty, precision_digits=precision) >= 0:
                continue

            group_id = line.order_id.procurement_group_id
            if not group_id:
                group_id = self.env['procurement.group'].create({
                    'name': line.order_id.name, 'move_type': line.order_id.picking_policy,
                    'sale_id': line.order_id.id,
                    'partner_id': line.order_id.partner_shipping_id.id,
                })
                line.order_id.procurement_group_id = group_id
            else:
                # In case the procurement group is already created and the order was
                # cancelled, we need to update certain values of the group.
                updated_vals = {}
                if group_id.partner_id != line.order_id.partner_shipping_id:
                    updated_vals.update({'partner_id': line.order_id.partner_shipping_id.id})
                if group_id.move_type != line.order_id.picking_policy:
                    updated_vals.update({'move_type': line.order_id.picking_policy})
                if updated_vals:
                    group_id.write(updated_vals)

            values = line._prepare_procurement_values(group_id=group_id)
            product_qty = line.product_uom_qty - qty

            procurement_uom = line.product_uom
            quant_uom = line.product_id.uom_id
            get_param = self.env['ir.config_parameter'].sudo().get_param
            if procurement_uom.id != quant_uom.id and get_param('stock.propagate_uom') != '1':
                product_qty = line.product_uom._compute_quantity(product_qty, quant_uom, rounding_method='HALF-UP')
                procurement_uom = quant_uom

            try:
                print ("values ",values)
                self.env['procurement.group'].run(line.product_id, product_qty, procurement_uom, line.order_id.partner_shipping_id.property_stock_customer, line.name, line.order_id.name, values)
            except UserError as error:
                errors.append(error.name)
        if errors:
            raise UserError('\n'.join(errors))
        return True


