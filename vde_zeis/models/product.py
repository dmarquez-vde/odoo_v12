# -*- coding: utf-8 -*-
# ##############################################################################
#

#    OpenERP, Open Source Management Solution
#    Copyright (C) 2012-Today Acespritech Solutions Pvt Ltd
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import models, fields, api, _
from odoo.addons import decimal_precision as dp
import datetime

class ProductTemplate(models.Model):
    _inherit = 'product.template'

    rental_subscription_product = fields.Boolean(
        string="Es un producto de renta"
    )

    property_stock_incoming = fields.Many2one(
        'stock.location', "Ubicación de entrada",
        company_dependent=True, domain=[('usage', 'like', 'internal')])

    property_stock_outgoing = fields.Many2one(
        'stock.location', "Ubicacion de salida",
        company_dependent=True, domain=[('usage', 'like', 'internal')])


    qty_sent = fields.Float(string="Existencia general", compute="_get_qty_sent")


    @api.one
    @api.depends('product_variant_id')
    def _get_qty_sent(self):
        quant_ids = self.env['stock.quant'].search([('product_id','=',self.product_variant_id.id)])
        if quant_ids:
            internal = 0
            external = 0
            for quant in quant_ids:
                if quant.location_id.usage == 'internal':
                    internal = internal + quant.quantity
                if quant.location_id.usage == 'customer':
                    external = external + quant.quantity
            self.qty_sent = external + internal

    @api.one
    @api.depends('product_variant_id')
    def _get_qty_sent_(self):
        move_ids = self.env['stock.move'].search([('product_id','=',self.product_variant_id.id),('state', '=','done'),('picking_type_id','=',False)])
        if move_ids:
            inv = 0
            for move in move_ids:
                inv = inv + move.product_qty
            inv = inv / len(move_ids)
        move_ids = self.env['stock.move'].search([('product_id','=',self.product_variant_id.id),('state', '=','done')])
        print ("move_id ", move_ids)
        if move_ids:
            entrada = 0
            salida = 0
            cont_s = 0
            cont_e = 0
            for move in move_ids:
                if move.picking_type_id.code=='outgoing':
                    salida = salida + move.product_qty
                    cont_s = cont_s + 1
                elif move.picking_type_id.code=='incoming':
                    entrada = entrada + move.product_qty
                    cont_e = cont_e + 1
            #self.qty_sent=entrada - salida + inv
            print ("salida ", salida)
            print ("salida ", cont_s)
            print ("entrada ", entrada)
            print ("entrada ", cont_e)
            print ("inv ", inv)
            #if cont_s:
                #self.qty_sent = salida / cont_s
            self.qty__sent = salida - entrada

