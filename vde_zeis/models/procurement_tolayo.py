# -*- coding: utf-8 -*-
# ##############################################################################
#

#    OpenERP, Open Source Management Solution
#    Copyright (C) 2012-Today Acespritech Solutions Pvt Ltd
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import models, fields, api, _
from odoo.addons import decimal_precision as dp
from odoo.exceptions import ValidationError, UserError
import datetime
import pytz

import logging
_logger = logging.getLogger(__name__)

class StockProductionLot(models.Model):

    _inherit = 'stock.production.lot'

    purchase = fields.Boolean(string="Para compra de proveedor?", default=False)

class ProcurementGroup(models.Model):

    _inherit = 'procurement.group'

    #def convert_to_utc(self, fecha):
        #to_zone = tz.gettz("UTC")
        #from_zone = tz.gettz("America/Mexico_City")
        #local = fecha.replace(tzinfo=from_zone)
        #utc = local.astimezone(to_zone)
        #return utc

    def cambio_fecha_hora_utc(self, fecha):
        timezone = pytz.timezone("America/Mexico_City")
        fecha_local = timezone.localize(fecha, is_dst=None)
        fecha_utc = fecha_local.astimezone(pytz.utc)
        return fecha_utc

    @api.model
    def run(self, product_id, product_qty, product_uom, location_id, name, origin, values):
        """ Method used in a procurement case. The purpose is to supply the
        product passed as argument in the location also given as an argument.
        In order to be able to find a suitable location that provide the product
        it will search among stock.rule.
        """
        line = self.env['sale.order.line'].browse(values.get('sale_line_id'))
        if line.order_id.is_rental == "True":
            planned_local = line.d1
            planned_utc = self.cambio_fecha_hora_utc(datetime.datetime(planned_local.year,planned_local.month,planned_local.day,9,00,00))
            planned_utc = datetime.datetime(planned_utc.year,planned_utc.month,planned_utc.day,planned_utc.hour,planned_utc.minute,planned_utc.second)
            values.setdefault('company_id', self.env['res.company']._company_default_get('procurement.group'))
            values.setdefault('priority', '1')
            values.setdefault('date_planned', planned_utc)
            values.update({'date_planned':planned_utc})
            rule = self._get_rule(product_id, location_id, values)
            if not rule:
                raise UserError(_('No procurement rule found in location "%s" for product "%s".\n Check routes configuration.') % (location_id.display_name, product_id.display_name))
            action = 'pull' if rule.action == 'pull_push' else rule.action
            if hasattr(rule, '_run_%s' % action):
                getattr(rule, '_run_%s' % action)(product_id, product_qty, product_uom, location_id, name, origin, values)
            else:
                _logger.error("The method _run_%s doesn't exist on the procument rules" % action)
            return True
        else:
            print ("No es renta")


    @api.model
    def _get_rule(self, product_id, location_id, values):
        """ Find a pull rule for the location_id, fallback on the parent
        locations if it could not be found.
        """
        #print ("values ", values)
        result = False
        domain = []
        location = location_id
        line = self.env['sale.order.line'].browse(values.get('sale_line_id'))
        if line.order_id.is_rental == True:
            if line.is_rental:
                location_src = line.product_id.property_stock_outgoing.id
                domain = [('location_id', '=', location.id),('action', '!=', 'push'),('location_src_id','=',location_src)]
            else:
                domain = [('location_id', '=', location.id), ('action', '!=', 'push')]
            print ("dominio ", domain)
            while (not result) and location:
                result = self._search_rule(values.get('route_ids', False), product_id, values.get('warehouse_id', False), domain)
                location = location.location_id
            print ("el result ", result)
            return result


class StockMove(models.Model):

    _inherit = 'stock.move'

    quote_id = fields.Many2one('sale.order.quote', related="sale_line_id.quote_id", string="Reservacion")