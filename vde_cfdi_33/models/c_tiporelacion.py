# -*- coding: utf-8 -*-
from openerp import fields,models,api,_
from datetime import date
import openerp.addons.decimal_precision as dp


class c_tiporelacion (models.Model):

    _name='c.tiporelacion'

    code = fields.Char(string='Tipo Relación',  required=True)
    name = fields.Char(string='Descripcion', required=True)
