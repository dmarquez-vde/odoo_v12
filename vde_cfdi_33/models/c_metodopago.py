# -*- coding: utf-8 -*-
from openerp import fields,models,api,_
from datetime import date
import openerp.addons.decimal_precision as dp


class c_metodopago (models.Model):

    _name='c.metodopago'

    code = fields.Char(string='Código',  required=True)
    name = fields.Char(string='Descripcion',  required=True)