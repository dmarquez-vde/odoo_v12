# -*- coding: utf-8 -*-
# ##############################################################################
#

#    OpenERP, Open Source Management Solution
#    Copyright (C) 2010-Vos Datos Enterprise Suite SA de CV
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import json
from lxml import etree
from datetime import datetime
from dateutil.relativedelta import relativedelta
from odoo import api, fields, models, tools,  _
from odoo.tools import float_is_zero
from odoo.tools.misc import formatLang
from odoo.exceptions import UserError, RedirectWarning, ValidationError
import odoo.addons.decimal_precision as dp


class WizardSoToSampleLine(models.TransientModel):
    _name = 'wizard.sotosample.line'

    wizard_id = fields.Many2one('wizard.sotosample', string='Wizard')
    product_id = fields.Many2one('product.product', string="Producto")
    qty = fields.Float(string="Cantidad")
    name = fields.Char(string="Descripcion")
    color = fields.Char(string="Color", help="Color hexadecimal")
    image = fields.Binary(string="Imagen",attachment=True,)
    image_medium = fields.Binary(string="Imagen", attachment=True)
    image_small = fields.Binary(string="Imagen", attachment=True)

    @api.model
    def create(self, vals):
        tools.image_resize_images(vals)
        return super(WizardSoToSampleLine, self).create(vals)

    @api.multi
    def write(self, vals):
        tools.image_resize_images(vals)
        return super(WizardSoToSampleLine, self).write(vals)

class WizardSoToSample(models.TransientModel):
    _name = 'wizard.sotosample'

    so_id = fields.Many2one('sale.order', 'Cotizacion')
    supplier_id = fields.Many2one('res.partner', string="Proveedor")
    partner_id = fields.Many2one('res.partner', string="Cliente",default=lambda self: self.env.context.get('partner_id') or False)
    contact_id = fields.Many2one('res.partner', string="Entrega/Factura")
    user_id = fields.Many2one('res.users', string="Solicitado por", default=lambda self: self.env.context.get('uid') or False)
    schedule = fields.Float(string="Horario de entrega")
    company_id = fields.Many2one('res.company', string="Compañia",default=lambda self: self.env.context.get('company_id') or False)
    line_ids = fields.One2many('wizard.sotosample.line', 'wizard_id', "Lineas")

    @api.model
    def default_get(self,fields):
        res = super(WizardSoToSample, self).default_get(fields)
        vals = []
        so_ids = self._context.get('active_ids')
        so_ids = self.env['sale.order'].browse(so_ids)
        for sale in so_ids:
            so_id = sale.id
            for line in sale.order_line:
                vals.append((0, 0, {
                    'product_id': line.product_id.id,
                    'qty': 1,
                    'name': line.product_id.name
                    }))
        res.update({'so_id':so_id,'line_ids':vals})
        return res

    @api.multi
    def create_sample(self):
        self.ensure_one()
        sample_obj = self.env['sale.sample']
        vals = []
        for line in self.line_ids:
            vals.append((0, 0, {
            'product_id': line.product_id.id,
            'qty': 1,
            'name': line.product_id.name,
            'color': line.color,
            'image_small': line.image_small
            }))
        sample_id = sample_obj.create({
            'sale_id': self.so_id.id,
            'partner_id': self.partner_id.id,
            'contact_id': self.contact_id.id,
            'supplier_id': self.supplier_id.id,
            'schedule': self.schedule,
            'requested_by': self.user_id.id,
            'line_ids': vals
            })
        if sample_id:
            #self.so_id.write({'sample_id':sample_id.id})
            return sample_id
        else:
            raise ValidationError('Ocurrio un error, intente nuevamente.')