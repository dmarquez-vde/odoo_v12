# -*- coding: utf-8 -*-
# ##############################################################################
#

#    OpenERP, Open Source Management Solution
#    Copyright (C) 2010-Vos Datos Enterprise Suite SA de CV
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import models, fields, api, _
from odoo.addons import decimal_precision as dp
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, float_compare
from odoo.exceptions import ValidationError, UserError
import datetime


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    @api.model
    def _get_quotation_sequence(self):
        seq_id = self.env['ir.sequence'].search([('code','=','purchase.order')],limit=1)
        if seq_id:
            return seq_id.id
        else:
            return False

    purchase_number = fields.Many2one('ir.sequence', string='Secuencia',required=True,
                                  #domain=[('code', 'in', ['purchase.order']),('company_id','=',self.company_id)],
                                  default=_get_quotation_sequence)


    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
            if vals.get('purchase_number'):
                obj_seq = self.env['ir.sequence'].browse(vals.get('purchase_number'))
                vals['name'] = obj_seq.next_by_id() or '/'
        return super(PurchaseOrder, self).create(vals)

