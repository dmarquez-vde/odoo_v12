# -*- coding: utf-8 -*-
# ##############################################################################
#

#    OpenERP, Open Source Management Solution
#    Copyright (C) 2010-Vos Datos Enterprise Suite SA de CV
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import json
from lxml import etree
from datetime import datetime
from dateutil.relativedelta import relativedelta
from odoo import api, fields, models, tools,  _
from odoo.tools import float_is_zero
from odoo.tools.misc import formatLang
from odoo.exceptions import UserError, RedirectWarning, ValidationError
import odoo.addons.decimal_precision as dp

class SaleSampleLine(models.Model):
    _name = 'sale.sample.line'
    _description = 'Linea de solicitud de muestra'

    sample_id = fields.Many2one('sale.sample', "Solicitud")
    qty = fields.Float(string="Cantidad")
    currency_id = fields.Many2one('res.currency', 'Currency', required=True, default=lambda self: self.env.user.company_id.currency_id.id)
    price = fields.Monetary(string="Costo", currency_field='currency_id', store=True, track_visibility='always')
    product_id = fields.Many2one('product.product', string="Producto")
    color = fields.Char(string="Color", help="Color hexadecimal")
    image = fields.Binary(string="Imagen",attachment=True,)
    image_medium = fields.Binary(string="Imagen", attachment=True)
    image_small = fields.Binary(string="Imagen", attachment=True)
    name = fields.Char(string="Descripcion")


    @api.model
    def create(self, vals):
        tools.image_resize_images(vals)
        return super(SaleSampleLine, self).create(vals)

    @api.multi
    def write(self, vals):
        tools.image_resize_images(vals)
        return super(SaleSampleLine, self).write(vals)



class SaleSample(models.Model):
    _name = 'sale.sample'
    _description = 'Solicitu de muestas'

    _inherit = ['mail.thread', 'mail.activity.mixin']

    state = fields.Selection(selection=[
     ('draft', 'Borrador'),
     ('confirmed', 'Confirmada'),
     ('pending', 'En proceso'),
     ('done', 'Terminada'),
     ('cancel', 'Cancelada'),
     ], default='draft', string="Estatus", store=True, copy=False)
    return_date = fields.Date(string="Fecha de devolucion")
    supplier_id = fields.Many2one('res.partner', string="Proveedor")
    partner_id = fields.Many2one('res.partner', string="Cliente")
    contact_id = fields.Many2one('res.partner', string="Entrega/Factura")
    line_ids = fields.One2many('sale.sample.line', 'sample_id', string="Muestras")
    requested_by = fields.Many2one('res.users', string="Solicitado por", default=lambda self: self.env.context.get('uid') or False)
    created_by = fields.Many2one('res.users', string="Realizado por")
    name = fields.Char("Numero")
    schedule = fields.Float(string="Horario de entrega")
    company_id = fields.Many2one('res.company', string="Compañia",default=lambda self: self.env.user.company_id.id)
    sale_id = fields.Many2one('sale.order', string='Cotizacion')

    @api.model
    def create(self, vals):
        vals['name'] = self.env['ir.sequence'].next_by_code('sale.sample')
        result = super(SaleSample, self).create(vals)
        return result

    #@api.multi
    #def write(self, vals):

        #result = super(SaleSample, self).write(vals)
        #if result:
            #if vals.get('state') == 'confirmed':

                    #return result
                #else:
                    #raise ValidationError('Ocurrio un error al enviar la notificacion.')
            #else:
                #return result

    @api.multi
    def set_done(self):
        local_context = self.env.context.copy()
        local_context.update({'from_user_name':self.env.user.name, 'from_user_email':self.env.user.email})
        template = self.env['ir.model.data'].get_object('vde_promolife', 'vde_promolife_sample_done_template')
        template = self.env['mail.template'].browse(template.id)
        if template.with_context(local_context).send_mail(self.id, force_send=True):
            return self.write({'state': 'done'})
        else:
            raise ValidationError('Ocurrio un error al enviar la notificacion.')

    @api.multi
    def set_confirmed(self):
        local_context = self.env.context.copy()
        params = self.env.context.get('params')
        model = self.env['ir.model'].search([('model','=',params.get('model'))])
        team_ids = self.env['mail.activity.team'].search([('res_model_ids','in',[model.id]),('company_id','=',self.env.user.company_id.id)])
        for team in team_ids:
            user = team.user_id
        if user:
            local_context.update({'to_user_name':user.name, 'to_user_email':user.email})
        template = self.env['ir.model.data'].get_object('vde_promolife', 'vde_promolife_sample_new_template')
        template = self.env['mail.template'].browse(template.id)
        if template.with_context(local_context).send_mail(self.id, force_send=True):
            return self.write({'state': 'confirmed'})
        else:
            raise ValidationError('Ocurrio un error al enviar la notificacion.')

    @api.multi
    def set_pending(self):
        local_context = self.env.context.copy()
        local_context.update({'from_user_name':self.env.user.name, 'from_user_email':self.env.user.email})
        template = self.env['ir.model.data'].get_object('vde_promolife', 'vde_promolife_sample_pending_template')
        template = self.env['mail.template'].browse(template.id)
        if template.with_context(local_context).send_mail(self.id, force_send=True):
            return self.write({'state': 'pending'})
        else:
            raise ValidationError('Ocurrio un error al enviar la notificacion.')

    @api.multi
    def set_cancel(self):
        return self.write({'state': 'cancel'})

    @api.multi
    def set_draft(self):
        return self.write({'state': 'draft'})


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    sample_ids = fields.One2many('sale.sample', 'sale_id', string="Solicitud de muestra")
    #sample_id = fields.Many2one('sale.sample', string="Solicitud de muestra")


