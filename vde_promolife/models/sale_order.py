# -*- coding: utf-8 -*-
# ##############################################################################
#

#    OpenERP, Open Source Management Solution
#    Copyright (C) 2010-Vos Datos Enterprise Suite SA de CV
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import json
from lxml import etree
from datetime import datetime
from dateutil.relativedelta import relativedelta
from odoo import api, fields, models, _
from odoo.tools import float_is_zero
from odoo.tools.misc import formatLang
from odoo.exceptions import UserError, RedirectWarning, ValidationError
import odoo.addons.decimal_precision as dp

class SaleOrder(models.Model):
    _inherit = 'sale.order'

    #personalization = fields.Selection(selection=[
    # ('1', 'SERIGRAFIA'),
    # ('2', 'TAMPOGRAFIA'),
    # ('3', 'GRABADO'),
    # ('4', 'HOT STAMPING'),
    # ('5', 'SUBLIMACION'),
    # ('6', 'FULL COLOR'),
    # ('7', 'BORDADO'),
    # ('8', 'GOTA DE RESINA'),
    # ('9', 'VINIL TEXTIL'),
    # ('10', 'FABRICACION'),
    # ], string="Personalización", store=True, copy=True)



    #proveedor = fields.Many2one('res.partner',string='Proveedor')

    #clave = fields.Char(string="Clave")


    horario_entrega = fields.Datetime(string="Horario de entrega")

    instrucciones = fields.Char(string="Instrucciones de entrega")

    maquila = fields.Char(string="Maquila, empaque o etiquetado")

    informacion = fields.Char(string="Informacion adicional")


    @api.model
    def _get_quotation_sequence(self):
        seq_id = self.env['ir.sequence'].search([('code','=','sale.order')],limit=1)
        if seq_id:
            return seq_id.id
        else:
            return False

    sale_number = fields.Many2one('ir.sequence', string='Secuencia',required=True,
                                  #domain=[('code', 'in', ['sale.order']),('company_id','=',self.company_id)],
                                  default=_get_quotation_sequence)


    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
            if vals.get('sale_number'):
                obj_seq = self.env['ir.sequence'].browse(vals.get('sale_number'))
                vals['name'] = obj_seq.next_by_id() or '/'
        return super(SaleOrder, self).create(vals)

class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    personalization = fields.Selection(selection=[
     ('1', 'SERIGRAFIA'),
     ('2', 'TAMPOGRAFIA'),
     ('3', 'GRABADO'),
     ('4', 'HOT STAMPING'),
     ('5', 'SUBLIMACION'),
     ('6', 'FULL COLOR'),
     ('7', 'BORDADO'),
     ('8', 'GOTA DE RESINA'),
     ('9', 'VINIL TEXTIL'),
     ('10', 'FABRICACION'),
     ], string="Personalización", store=True, copy=True)

    proveedor = fields.Many2one('res.partner',string='Proveedor')

    clave = fields.Char(string="Clave")

    logo = fields.Char(string="Logo")

    costo_maquila = fields.Float(string="Costo maquila")

    costo_limpio = fields.Float(string="Costo de producto limpio")

    importacion = fields.Selection(selection=[
     ('1', 'Nacional' ),
     ('2', 'Importación')], default= '1',string="Tipo de pedido")
