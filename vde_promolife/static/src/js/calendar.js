odoo.define('vde_zeis.CalendarView', function (require) {
"use strict";

    var CalendarView = require('web.CalendarView');
	console.log("aqui");
    CalendarView.include({
        init: function (viewInfo, params) {
            this._super.apply(this, arguments);
            if (this.controllerParams.modelName == 'sale.order.quote') {
                this.loadParams.editable = false;
                this.loadParams.creatable = false;
            }
        },
    });

    return CalendarView;
});
